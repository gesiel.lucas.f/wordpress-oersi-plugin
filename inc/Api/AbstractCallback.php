<?php
/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Api;

use Inc\Base\BaseController;

abstract class AbstractCallback extends BaseController
{


    /**
     *  Return Templates for the translations page.
     *
     * @since  1.0.0
     * @return void
     */
    abstract public function template();


    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */
    abstract public function fields();


    /**
     * function is called as a callback in setSection function.
     *
     * @since  1.0.0
     * @return void
     */
    abstract public function sectionManager();


    /**
     * This method will sanitize the input data before saving it in the database.
     *
     * @since  1.0.0
     * @param  array $input contains all input fields declared in the fields() method.
     * @return array $Output contains the sanitized inputs
     */
    public function sanitizeInputs($input)
    {
        $output = [];
        foreach ($this->fields() as $key) {
            if (isset($input[$key['name']])) {
                $output[$key['name']] = $input[$key['name']];

                // // if the input is a checkbox, we need to check if it is checked or not.
                // if ($key['type'] == 'checkbox') {
                // $output[$key['name']] = isset($input[$key['name']]) ? 1 : 0;
                // }
                // // if the input is a radio button, we need to check if it is checked or not.
                // elseif ($key['type'] == 'radio') {
                // $output[$key['name']] = isset($input[$key['name']]) ? 1 : 0;
                // }
                // // if the input is a number, we need to check if it is a number or not.
                // if ($key['type'] == 'number') {
                // $output[$key['name']] = is_numeric($input[$key['name']]) ? $input[$key['name']] : 0;
                // }
                // // if the input is a text, we need to check if it is a text or not.
                // if ($key['type'] == 'text') {
                // $output[$key['name']] = is_string($input[$key['name']]) ? strip_tags(stripslashes($input[$key['name']])) : '';
                // }
                // // if the input is a textArea, we need to check if it is a textArea or not.
                // if ($key['type'] == 'textArea') {
                // $output[$key['name']] = strip_tags(stripslashes($input[$key['name']]));
                // }
            } else {
                $output[$key['name']] = "";
            }//end if
        }//end foreach
        return $output;
    }


    /**
     * This method with deliver the input based on type of input.
     *
     * @since 1.0.0
     *
     * @param  array $args contains the input fields and their attributes.
     * @return void
     */
    abstract public function render($args);


}
