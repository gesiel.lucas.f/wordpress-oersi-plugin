<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Api;

class SettingsApi
{

    /**
     * A variable to store the admin pages slug
     * @var array
     */
    public $adminpages = [];

    /**
     * A variable to store the admin sub pages
     * @var array
     */
    public $adminSubPages = [];

    /**
     * A variable to store the settings sections
     * @var array
     */
    public $settings = [];

    /**
     * A variable to store the sections
     * @var array
     */
    public $sections = [];

    /**
     * A variable to store the fields
     * @var array
     */
    public $fields = [];


    /**
     * A function that is used to register the admin pages,subPages,settings,sections and fields
     *  This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        if (!empty($this->adminpages) || !empty($this->adminSubPages)) {
            add_action('admin_menu', [$this, 'addAdminMenu']);
        }

        if (!empty($this->settings)) {
            add_action('admin_init', [$this, 'registerCustomFields']);
        }
    } //end register()


    /**
     * Since $pages is private, we use this function to set $pages from the outside
     * @param array $pages A list of pages to be added to the admin menu
     * @return $this
     */
    public function addPages(array $pages)
    {
        $this->adminpages = $pages;

        return $this;
    } //end addPages()


    /**
     * Since $subPages is private, we use this function to set $subPages from the outside
     * @param String Use to add a title for the sub page, if we don't want to add a sub page,
     * it will create a sub page with the title
     * @return $this
     */
    public function withSubPage(string $title = null)
    {
        if (empty($this->adminpages)) {
            return $this;
        }

        $adminPage = $this->adminpages[0];

        $subpage = [
            [
                'parent_slug' => $adminPage['menu_slug'],
                'page_title'  => $adminPage['page_title'],
                'menu_title'  => ($title) ? $title : $adminPage['menu_title'],
                'capability'  => $adminPage['capability'],
                'menu_slug'   => $adminPage['menu_slug'],
                'callback'    => $adminPage['callback'],
            ],
        ];

        $this->adminSubPages = $subpage;

        return $this;
    } //end withSubPage()


    /**
     * Since $subPages is private, we use this function to set $subPages from the outside
     * @param array $subPages A list of sub pages to be added to the admin menu
     * @return $this
     */
    public function addSubPages(array $subPages)
    {
        $this->adminSubPages = array_merge($this->adminSubPages, $subPages);

        return $this;
    } //end addSubPages()


    /**
     * This function will add the pages and sub pages to the admin menu
     * @return void
     */
    public function addAdminMenu()
    {
        foreach ($this->adminpages as $page) {
            add_menu_page(
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback'],
                $page['icon_url'],
                $page['position']
            );
        }

        foreach ($this->adminSubPages as $page) {
            add_submenu_page(
                $page['parent_slug'],
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback']
            );
        }
    } //end addAdminMenu()


    /**
     * Since $settings is private, we use this function to set $settings from the outside
     * @param array $settings A list of settings to be added to the admin menu
     * @return $this
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;

        return $this;
    } //end setSettings()


    /**
     * Since $sections is private, we use this function to set $sections from the outside
     * @param array $sections A list of sections to be added to the admin menu
     * @return $this
     */
    public function setSections(array $sections)
    {
        $this->sections = $sections;

        return $this;
    } //end setSections()


    /**
     * Since $fields is private, we use this function to set $fields from the outside
     * @param array $fields A list of fields to be added to the admin menu
     * @return $this
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;

        return $this;
    } //end setFields()


    /**
     * This function will register the settings,sections and fields
     * @return void
     */
    public function registerCustomFields()
    {
        // Register setting.
        foreach ($this->settings as $setting) {
            register_setting(
                $setting["option_group"],
                $setting["option_name"],
                (isset($setting["callback"]) ? $setting["callback"] : '')
            );
        }

        // Add settings section.
        foreach ($this->sections as $section) {
            add_settings_section(
                $section["id"],
                $section["title"],
                (isset($section["callback"]) ? $section["callback"] : ''),
                $section["page"]
            );
        }

        // Add settings field.
        foreach ($this->fields as $field) {
            add_settings_field(
                $field["id"],
                $field["title"],
                (isset($field["callback"]) ? $field["callback"] : ''),
                $field["page"],
                $field["section"],
                (isset($field["args"]) ? $field["args"] : '')
            );
        }
    } //end registerCustomFields()


}//end class
