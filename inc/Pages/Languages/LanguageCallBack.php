<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Translation;

use Inc\Api\AbstractCallback;

class LanguageCallBack extends AbstractCallback
{

    /**
     * Declare the option group, for setSettings() method
     *
     * @var string
     */
    public static $optionGroup = 'oersi_plugin_language';

    /**
     * Declare the option name, for setSettings() method
     *
     * @var string
     */
    public static $optionName = 'oersi_plugin_language';

    /**
     * Declare the Id for setSettings() method
     *
     * @var string
     */
    public static $sectionId = 'oersi_plugin_language_index';


    /**
     *  Return the Html template, this function is called as a callback in the setPage function.
     *
     * @since  1.0.0
     * @return void
     */
    public function template()
    {
        return require_once("$this->pluginPath/templates/language/languages.php");
    }


    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */
    public function fields()
    {
        return [
            [
                'name'         => 'language',
                'title'        => __('Add multiple languages below', 'oersi-domain'),
                'class'        => 'oersi-ui-oersi_plugin_language',
                'data-tooltip' => __('You can add multiple languages in the Json format ex: {"en":{},"fr":{}}', 'oersi-domain'),
                'type'         => 'textArea',
            ],

        ];
    }


    /**
     *  Text For one section of the elasticsSearch page.
     *
     * @since  1.0.0
     * @return void
     */
    public function sectionManager()
    {
        echo __('Manage the sections of the Lang page', 'oersi-domain');
    }


    /**
     *  This method will generate a textarea for working with Json fields.
     */
    public function renderTexArea($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-area';
        $id = ($args['id'] ?? $name).'-ui-text-area-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        $renderHtml = '<textarea id="translation" style="display: none !important;"  hidden name="'.$optionName.'['.$name.']"'.'class="'.$classes.' id="'.$id.'" />'.(isset($value[$name]) ? $value[$name] : '').'</textarea>';
        echo $renderHtml;
    } //end renderTexArea()


    /**
     * This method with deliver the input based on type of input.
     *
     * @since  1.0.0
     * @return void
     */
    public function render($args)
    {
        $this->renderTexArea($args);
    } //end render()


}//end class LanguageCallBack
