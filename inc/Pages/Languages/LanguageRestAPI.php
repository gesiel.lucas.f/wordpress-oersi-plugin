<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Translation;

use Inc\Api\RestAPI;
use Inc\Pages\Translation\LanguageCallBack;

/**
 *
 */
class LanguageRestAPI extends LanguageCallBack
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;


    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();

    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */

    public function setApiRoutes()
    {
        // $this->baseRoute = 'oersi/v1';
        // give a parameter ?lang=en to the url to get the translation for that language
        $this->apiRoutes = [
            [
                'endpoint' => 'language',
                'methods' => "GET",
                'permission_callback' => "__return_true",
                'callback' => [$this, 'getTranslation']
            ]
        ];
    } //end setApiRoutes()

    /**
     *  A callback function that we can use to get the elasticssearch config
     * it will return the elasticssearch config
     * @return array
     */
    public function getTranslation(\WP_REST_Request $request){
        $lang = $request->get_param('lang');
        $ns = $request->get_param('ns');
        $fieldName = self::fields()[0]['name'];
        $translation = get_option(self::$optionName)[$fieldName];
        If (empty($translation)) {
            return new \WP_Error('rest_not_found', __('No Translations found', 'oersi-domain'), array('status' => 404));
        }

        // convert in
        $translation = json_decode($translation, true);
        $translation = $translation[$lang][$ns];
        $result= new \WP_REST_Response($translation, 200);
        $result->set_headers(array('Cache-Control'=>'max-age=3600'));
        return $result;
    }


}//end class
