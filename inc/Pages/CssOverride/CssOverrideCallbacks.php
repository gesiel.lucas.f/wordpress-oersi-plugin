<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Pages\CssOverride;

use Inc\Api\AbstractCallback;

class CssOverrideCallbacks extends AbstractCallback
{

    /**
     * Declare the option group, for setSettings() method
     *
     * @var string
     */
    public static $optionGroup = 'oersi_plugin_css';

    /**
     * Declare the option name, for setSettings() method
     *
     * @var string
     */
    public static $optionName = 'oersi_plugin_css';

    /**
     * Declare the Id for setSettings() method
     *
     * @var string
     */
    public static $sectionId = 'oersi_css_index';


    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */
    public function fields()
    {
        return [
            [
                'name'         => 'css',
                'title'        => __('Css override', 'oersi-domain'),
                'class'        => 'oersi-ui-css_override',
                'placeholder'  => __('exp: .oersi-ui-css_override { color: red; }', 'oersi-domain'),
                'data-tooltip' => __('You can override the css of the plugin by adding your own css here.', 'oersi-domain'),
                'type'         => 'textArea',
            ],
        ];
    } //end fields()


    /**
     *  Return the Html template, this function is called as a callback in the setPage function.
     *
     * @since  1.0.0
     * @return void
     */
    public function template()
    {
        return include_once "$this->pluginPath/templates/cssOverride/cssOverride.php";
    } //end template()


    /**
     *  Text For one section of the elasticsSearch page.
     *
     * @since  1.0.0
     * @return void
     */
    public function sectionManager()
    {
        echo __('To override the CSS of the plugin, you can add your own css here. every class that start with prefix "'.$_ENV["OERSI_PLUGIN_CLASS_PREFIX"].'" can be override.', 'oersi-domain');
    } //end sectionManager()


    /**
     * This method with deliver the input based on type of input.
     *
     * @since  1.0.0
     * @return void
     */
    public function render($args)
    {
        $this->textAreaField($args);
    } //end render()


    /**
     *  This method will generate a textarea field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     * @return void
     */
    public function textAreaField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-area';
        $id = ($args['id'] ?? $name).'-ui-text-area-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        $renderHtml = '<textarea id="css_override" style="display: none !important;"  hidden name="'.$optionName.'['.$name.']"'.'class="'.$classes.' id="'.$id.'" />'.(isset($value[$name]) ? $value[$name] : '').'</textarea>';
        echo $renderHtml;
    } //end textAreaField()


}//end class
