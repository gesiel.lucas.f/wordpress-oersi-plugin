<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\CssOverride;

use Inc\Api\RestAPI;
use Inc\Pages\CssOverride\CssOverrideCallbacks;

/**
 *
 */
class CssOverrideRestAPI extends CssOverrideCallbacks
{

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */


    public function setApiRoutes()
    {
        $this->apiRoutes = [
            [
                'endpoint'            => 'cssoverride',
                'methods'             => \WP_REST_Server::READABLE,
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getCssOverride',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     *  A callback function that we can use to get the elasticssearch config
     * it will return the elasticssearch config
     * @return array
     */
    public function getCssOverride($request)
    {
        $cssOverride = get_option(self::$optionName);
        if (empty($cssOverride)) {
            return new \WP_Error('rest_not_found', __('No css override found', 'oersi-domain'), ['status' => 404]);
        }
        return $cssOverride['css'];
    }


}//end class
