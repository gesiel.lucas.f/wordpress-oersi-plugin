<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\EduSharing;

use Inc\Api\RestAPI;
use Inc\Base\BaseController;
use Inc\Pages\Configuration\ConfigurationCallbacks;

/**
 *
 */
class CollectionApi extends BaseController
{

    const BASE_PART = '/edu-sharing/rest';

    /**
     * A property to store the rest api class
     *
     * @var restAPI
     */
    public $restAPI;

    /**
     * A variable to store base route
     * @var string
     */
    public $baseRoute = '';

    /**
     * A variable to store the api routes
     * @var array
     */
    public $apiRoutes = [];

    public $apiRoutessss = stdClass::class;


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->restAPI = new RestAPI();
        $this->setApiRoutes();
        $this->restAPI->setApiRoutes($this->apiRoutes)->register();
    } //end register()


    /**
     *  A function that we can add all the methods we want to add to the wordpress rest api
     *  it will assign the pages to the $this->apiRoutes array
     *
     * @return void
     */
    public function setApiRoutes()
    {
        $this->apiRoutes = [
            [
                'endpoint'            => 'collections',
                'methods'             => "GET",
                'permission_callback' => "__return_true",
                'callback'            => [
                    $this,
                    'getCollections',
                ],
            ],
        ];
    } //end setApiRoutes()


    /**
     *  A callback function that we can use to get the vocabularies list
     * it will return the json with the vocabularies list and language
     * @return array
     */
    public function getCollections(\WP_REST_Request $request)
    {
        $configuration = get_option(ConfigurationCallbacks::$optionName);

        if (empty($configuration) || !is_array($configuration)) {
            return new \WP_Error('System Error', 'The system is not configured yet', ['status' => 500]);
        }

        if (empty($configuration['collectionUrl'])) {
            return new \WP_Error('Missing URL', 'Please add the domain name of the collection', ['status' => 500]);
        }

        $eduSharingClient = new EduSharingRestClient($configuration['collectionUrl']);

        $queryParams = [
            'scope'          => 'EDU_ALL',
            'maxItems'       => (!empty($configuration['collectionLength']) ? $configuration['collectionLength'] : 20),
            'skipCount'      => 0,
            'sortProperties' => 'cm:modified',
            'sortAscending'  => 'false',
        ];

        $responseData = $eduSharingClient->get(
            self::BASE_PART.'/collection/v1/collections/-home-/-root-/children/collections',
            $queryParams
        );

        if ($responseData->getStatusCode() === 200) {
            $responseArray = json_decode($responseData->getBody()->getContents(), true);
        }

        $collection = $this->buildResponse(
            $responseArray,
            $configuration['collectionUrl']
        );
        return new \WP_REST_Response(($collection ?? []), 200);
    } //end getConfiguration()


    /**
     *  A a function that can remove nested nested array from the array,  it will return a flat array
     * it will return a flat array
     * @param array $array - the array that we want to flatten
     * @return array
     */
    private function buildResponse($collections, $collectionUrl)
    {
        $newCollection = [
            'collections' => [],
            'pagination'  => [],
        ];
        if (empty($collections['collections'])) {
            return [];
        }
        foreach ($collections['collections'] as $collection) {
            array_push($newCollection['collections'], [
                'id'                    => $collection['ref']['id'],
                'name'                  => $collection['name'],
                'title'                 => $collection['title'],
                'color'                 => $collection['collection']['color'],
                'preview'               => $collection['preview']['url'],
                'description'           => $collection['collection']['description'],
                'childCollectionsCount' => $collection['collection']['childCollectionsCount'],
                'childReferencesCount'  => $collection['collection']['childReferencesCount'],
                'createdBy'             => $collection['createdBy']['firstName'].' '.$collection['createdBy']['lastName'],
                'createdAt'             => $collection['createdAt'],
                'modifiedAt'            => $collection['modifiedAt'],
                'link'                  => $collectionUrl.'/edu-sharing/components/collections?id='.$collection['ref']['id'],

            ]);
        }
        if (!empty($collections['pagination'])) {
            array_push($newCollection['pagination'], $collections['pagination']);
        }
        return $newCollection;
    } //end flatten()


}//end class
