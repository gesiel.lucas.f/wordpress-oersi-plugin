<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Pages\Configuration;

use Inc\Api\AbstractCallback;

class ConfigurationCallbacks extends AbstractCallback
{

    /**
     * Declare the option group, for setSettings() method
     *
     * @var string
     */
    public static $optionGroup = 'oersi_plugin_configuration';

    /**
     * Declare the option name, for setSettings() method
     *
     * @var string
     */
    public static $optionName = 'oersi_plugin_configuration';

    /**
     * Declare the Id for setSettings() method
     *
     * @var string
     */
    public static $sectionId = 'oersi_configuration';



    /**
     *  Fields to be generated in the settings page, this function is called in the register method.
     *
     * @since  1.0.0
     * @return array
     */


    public function fields()
    {
        return [
            [
                'name'         => 'isCollectionActive',
                'id'           => 'isCollectionActive',
                'title'        => __('Activate Twillo collection', 'oersi-domain'),
                'class'        => 'oersi-ui-checkbox-isCollectionActive',
                'data-tooltip' => __('Enable to show the twillo collection in search  ( see Documentation )', 'oersi-domain'),
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'collectionUrl',
                'id'           => 'collectionUrl',
                'title'        => __('URL of collection', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-collectionLength',
                'data-tooltip' => __('Give the URL of edu-sharing API for collection  ( see Documentation )', 'oersi-domain'),
                'type'         => 'text',
                'placeholder'  => 'exp: https://www.twillo.de/',
            ],
            [
                'name'         => 'collectionLength',
                'id'           => 'collectionLength',
                'title'        => __('Choose the length of collection', 'oersi-domain'),
                'default'      => '10',
                'class'        => 'oersi-ui-number-field-collectionLength',
                'data-tooltip' => __('You can decide the length of collection to be showed  ( see Documentation )', 'oersi-domain'),
                'type'         => 'number',
            ],
            [
                'name'         => 'pageSize',
                'id'           => 'pageSize',
                'title'        => __('Choose the default page size', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-pageSize',
                'data-tooltip' => __('You can decide the default page size  ( see Documentation )', 'oersi-domain'),
                'default'      => '10',
                'type'         => 'number',
            ],
            [
                'name'         => 'pageSizeOptions',
                'id'           => 'pageSizeOptions',
                'title'        => __('Choose the page size options', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-pageSizeOptions',
                'data-tooltip' => __('You can decide the page size options  ( see Documentation )', 'oersi-domain'),
                'placeholder'  => '10,20,30,...',
                'default'      => '12,24,36',
                'type'         => 'text',
            ],
            [
                'name'         => 'showFirstLastButtons',
                'id'           => 'showFirstLastButtons',
                'title'        => __('Show first and last buttons', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-showFirstLastButtons',
                'data-tooltip' => __('You can decide to show first and last buttons  ( see Documentation )', 'oersi-domain'),
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'showProgressBar',
                'id'           => 'showProgressBar',
                'title'        => __('Choose the show progress bar', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-showProgressBar',
                'data-tooltip' => __('You can decide the show progress bar  ( see Documentation )', 'oersi-domain'),
                // 'default'        => 'checked',
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'loadingFull',
                'id'           => 'loadingFull',
                'title'        => __('Select if you want to show the loading full', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-loadingFull',
                'data-tooltip' => __('You can decide the show loading full  ( see Documentation )', 'oersi-domain'),
                // 'default'        => 'checked',
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'showHtmlTags',
                'id'           => 'showHtmlTags',
                'title'        => __('Select if you want to show the html tags', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-showHtmlTags',
                'data-tooltip' => __('You can decide the show html tags  ( see Documentation )', 'oersi-domain'),
                // 'default'        => 'checked',
                'type'         => 'checkbox',
            ],
            [
                'name'         => 'searchTextLength',
                'id'           => 'searchTextLength',
                'title'        => __('Select length of the text you want to show in search and Selected filters ( see Documentation )', 'oersi-domain'),
                'class'        => 'oersi-ui-number-field-showHsearchTextLengthtmlTags',
                'data-tooltip' => __('You can decide the show html tags  ( see Documentation )', 'oersi-domain'),
                'default'      => '20',
                'type'         => 'number',
            ],
            [
                'name'         => 'viewTypeFrontEnd',
                'id'           => 'viewTypeFrontEnd',
                'title'        => __('Choose the view type you want to show as default in front end (List/Card)', 'oersi-domain'),
                'class'        => 'oersi-ui-select-field-viewTypeFrontEnd',
                'data-tooltip' => __('You can decide the default view type (List/Card)', 'oersi-domain'),
                'default'      => 'card',
                'type'         => 'select',
                'options'      => [
                    'list' => __('List', 'oersi-domain'),
                    'card' => __('Card', 'oersi-domain'),
                ],
            ],
            [
                'name'         => 'cardNumberOptions',
                'id'           => 'cardNumberOptions',
                'title'        => __('Select the number of cards to be displayed horizontally ( see Documentation )', 'oersi-domain'),
                'class'        => 'oersi-ui-number-card-showHCards',
                'data-tooltip' => __('You can choose only (2,3,4,5 and 6)  ( see Documentation )', 'oersi-domain'),
                'default'      => '2',
                'type'         => 'select',
                'conditions'   => [
                    'relation' => 'and',
                    'terms'    => [
                        [
                            'name'     => 'viewTypeFrontEnd',
                            'operator' => '===',
                            'value'    => 'card',
                        ],
                    ],
                ],
                'options'      => [
                    '2' => __('2 (two)', 'oersi-domain'),
                    '3' => __('3 (three)', 'oersi-domain'),
                    '4' => __('4 (four)', 'oersi-domain'),
                    '5' => __('5 (five)', 'oersi-domain'),
                    '6' => __('6 (six)', 'oersi-domain'),
                ],
            ],
        ];
    } //end fields()


    /**
     *  Return the Html template, this function is called as a callback in the setPage function.
     *
     * @since  1.0.0
     * @return void
     */
    public function template()
    {
        return include_once "$this->pluginPath/templates/configuration/configuration.php";
    } //end template()


    /**
     *  function is called as a callback in setSection function.
     *
     * @since  1.0.0
     * @return void
     */
    public function sectionManager()
    {
        echo __('Manage the sections of configurations', 'oersi-domain');
    } //end sectionManager()


    /**
     * This method with deliver the input based on type of input.
     *
     * @since  1.0.0
     *
     * @param  string $args contains input arguments
     * @return void
     */
    public function render($args)
    {
        $type = ($args['type'] ?? 'text');
        if ($type == 'text') {
            $this->textField($args);
        } elseif ($type == 'textArea') {
            $this->textAreaField($args);
        } elseif ($type == 'checkbox') {
            $this->checkboxField($args);
        } elseif ($type == 'number') {
            $this->numberField($args);
        } elseif ($type == 'select') {
            $this->dropDownField($args);
        }
    } //end render()


    /**
     *  This method will generate a text field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-field';
        $placeholder = ($args['placeholder'] ?? '');
        $default = ($args['default'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-text-field-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        echo '<input type="text" name="'.$optionName.'['.$name.']"'.' class="'.$classes.'" size="100" placeholder="'.$placeholder.'" value="'.(isset($value[$name]) ? $value[$name] : $default).'" id="'.$id.'" />';
    } //end textField()


    /**
     *  This method will generate a textarea field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function textAreaField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-text-area';
        $id = ($args['id'] ?? $name).'-ui-text-area-id';
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        $renderHtml = '<textarea id="configuration_text_area" style="display: none !important;" hidden name="'.$optionName.'['.$name.']"'.' id="'.$id.'" class="'.$classes.'" />'.(isset($value[$name]) ? $value[$name] : '').'</textarea>';
        echo $renderHtml;
    } //end textAreaField()


    /**
     *  This method will generate a checkbox field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function checkboxField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-checkbox-field';
        $default = ($args['default'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-checkbox-field-id';
        $optionName = $args['option_name'];
        $checkbox = get_option($optionName);
        echo '<div class="'.$classes.'"><input type="checkbox" id="'.$id.'" name="'.$optionName.'['.$name.']" class="" '.($checkbox[$name] ? 'checked' : $default).'><label for="'.$name.'"><div></div></label></div>';
    } //end checkboxField()


    /**
     *  This method will generate a input number field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function numberField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-number-field';
        $id = ($args['id'] ?? $name).'-ui-number-field-id';
        $default = ($args['default'] ?? '10');
        $optionName = $args['option_name'];
        $value = get_option($optionName);
        echo '<div class="'.$classes.'"><input type="number" id="'.$id.'" name="'.$optionName.'['.$name.']" value="'.(isset($value[$name]) ? $value[$name] : $default).'" class="'.$classes.'"  min="1" max="100"><label for="'.$name.'"><div></div></label></div>';
    } //end checkboxField()


    /**
     *  This method will generate a select field. and will be called as a callback in the renderInputs method.
     *
     * @since  1.0.0
     *
     * @param  array $args contains input arguments
     * @return void
     */
    public function dropDownField($args)
    {
        $name = $args['label_for'];
        $classes = ($args['class'] ?? $name).'-ui-select-field';
        $default = ($args['default'] ?? '');
        $id = ($args['id'] ?? $name).'-ui-select-field-id';
        $optionName = $args['option_name'];
        $select = get_option($optionName);
        $select = ($select[$name] ?? $default);
        $options = ($args['options'] ?? []);


        $buildHtmlTag  = '<div class="'.$classes.'">';
        $buildHtmlTag .= '<select name="'.$optionName.'['.$name.']" id="'.$id.'">';

        foreach ($options as $key => $value) {
            $buildHtmlTag  .= '<option value="'.$key.'" '.($select == $key ? 'selected' : '').'>'.$value.'</option>';
        }
        $buildHtmlTag  .= '</select>';
        $buildHtmlTag  .= '<label for="'.$name.'"><div></div></label></div>';
        echo $buildHtmlTag;
    }//end checkboxField()


}//end class
