<?php

/**
 * Created on Thu Jul 07 2022
 *
 * @package  OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Pages\Configuration;

use Inc\Api\SettingsApi;
use Inc\Base\Utils;
use Inc\Pages\Configuration\ConfigurationCallbacks;

/**
 *
 */
class Configuration extends ConfigurationCallbacks
{

    /**
     * A property to store the settings api class
     *
     * @var SettingsApi
     */
    public $settings;

    /**
     * A property to store an array of all the pages we want to add to the wordpress admin menu
     *
     * @var array
     */
    public $pages = [];


    /**
     *  A function that is called when we register it in the init method
     *  Register contain all the methods that we want to call when we register the plugin
     *
     * @return void
     */
    public function register()
    {
        $this->settings = new SettingsApi();

        $this->setPages();
        $this->setSettings();
        $this->setSections();
        $this->setFields();

        $this->settings->addPages($this->pages)->withSubPage(__('Configuration', 'oersi-domain'))->register();
    } //end register()


    /**
     *  A function that we can add all the pages we want to add to the wordpress admin menu
     *  it will assign the pages to the $this->pages array
     *
     * @return void
     */
    public function setPages()
    {
        $this->pages = [
            [
                'page_title' => __('OERSI', 'oersi-domain'),
                'menu_title' => __('OERSI', 'oersi-domain'),
                'capability' => 'manage_options',
                'menu_slug'  => 'oersi_plugin',
                'callback'   => [
                    $this,
                    'template',
                ],
                'icon_url'   => $this->pluginAssets.'images/oer-icon.svg',
                'position'   => 110,
            ],
        ];
    } //end setPages()


    /**
     *  A function that registers all the settings we want to add to the wordpress admin menu
     *  it will assign the settings to the $this->settings
     *
     * @return void
     */
    public function setSettings()
    {
        $args = [
            [
                'option_group' => self::$optionGroup,
                'option_name'  => self::$optionName,
                'callback'     => [
                    $this,
                    'sanitizeInputs',
                ],
            ],
        ];

        $this->settings->setSettings($args);
    } //end setSettings()


    /**
     *  Part of the Settings API. this  define all the sections we want to add  at the top of the settings section before the actual fields.
     *
     *  it will assign the sections to the $this->sections
     *
     * @return void
     */
    public function setSections()
    {
        $args = [
            [
                'id'       => self::$sectionId,
                'title'    => __('Configuration', 'oersi-domain'),
                'callback' => [
                    $this,
                    'sectionManager',
                ],
                'page'     => self::$optionName,
            ],
        ];

        $this->settings->setSections($args);
    } //end setSections()


    /**
     *  Part of the Settings API. this  define all the fields we want to add to the settings section.
     *  it will assign the fields to the $this->fields
     *
     * @return void
     */
    public function setFields()
    {
        // Get the registered option values from WordPress.
        $optionValues = get_option(self::$optionName);

        $args = [];
        foreach ($this->fields() as $key) {
            if (Utils::are_conditions_met(isset($key['conditions']) ? $key['conditions'] : null, $optionValues)) {
                $args[] = [
                    'id'       => $key['name'],
                    'title'    => $key['title'],
                    'callback' => [
                        $this,
                        'render',
                    ],
                    'page'     => self::$optionName,
                    'section'  => self::$sectionId,
                    'args'     => [
                        'label_for'    => $key['name'],
                        'class'        => isset($key['class']) ? $key['class'] : '',
                        'placeholder'  => isset($key['placeholder']) ? $key['placeholder'] : '',
                        'option_name'  => self::$optionName,
                        'type'         => isset($key['type']) ? $key['type'] : 'text',
                        'data-tooltip' => isset($key['data-tooltip']) ? $key['data-tooltip'] : '',
                        'id'           => isset($key['id']) ? $key['id'] : '',
                        'default'      => isset($key['default']) ? $key['default'] : '',
                        'options'      => isset($key['options']) ? $key['options'] : [],
                    ],
                ];
            } else {
                unset($optionValues[$key['name']]);
                update_option(self::$optionName, $optionValues, true);
            }//end if
        }//end foreach

        $this->settings->setFields($args);
    } //end setFields()


}//end class
