<?php

/**
 * Created on Fri August 04 2023
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2023 TIB <https://www.tib.eu/en>
 */

 namespace Inc\Pages\Vocabularies;

use Inc\RestClient\RestClient;
use InvalidArgumentException;

class VocabulariesRestClient extends RestClient
{

    private $baseUrl;


    public function __construct($vocabularyBaseUrl)
    {
        if (empty($vocabularyBaseUrl)) {
            throw new InvalidArgumentException("Base URL must be provided.");
        }
        $this->baseUrl = $vocabularyBaseUrl;
        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function getClientConfig(): array
    {

        return [
            'base_uri' => $this->baseUrl,
        ];
    }


}
