<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */


namespace Inc\Base;

use Inc\Base\BaseController;

/**
 *
 */
class EnqueueCss extends BaseController
{

    /**
     * Private Variable to store the css file name and path to the css file
     * @var array
     */
    private static $css_files = [
        [
            'handle'  => 'main_style',
            'src'     => 'assets/css/oer-index.css',
            'deps'    => false,
            'version' => false,
            'media'   => 'all',
        ],
    ];


    /**
     * A function that is used to register the Action Hooks,
     * This function is called from the register function in the init class
     * @return void
     */
    public function register()
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueue']);
         // I'm gonna comment it  for now? add_action('wp_enqueue_scripts', [$this, 'registerFrontEndCss']);?
    } //end register()


    /**
     * A function that is used to register the css files, if the css file is not registered otherwise
     * it will not be registered again
     * @return void
     */
    public function enqueue()
    {
        // Enqueue all our css files.
        foreach (self::$css_files as $css_file) {
            wp_enqueue_style(
                $css_file['handle'],
                $this->pluginUrl.$css_file['src'],
                $css_file['deps'],
                $css_file['version'],
                $css_file['media']
            );
        }
    } //end enqueue()


    /**
     * A function that will register the css files for the front end
     * it will not be registered again
     * @return void
     */
    public function registerFrontEndCss()
    {
        wp_register_style(
            'frontend_style',
            $this->pluginUrl.'assets/css/oer-frontend.css',
            [],
            '1.0.0',
            false
        );
        wp_enqueue_style('frontend_style');
    } //end registerFrontEndCss()


}//end class
