<?php

/**
 * Created on Fri Jul 08 2022
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

namespace Inc\Base;

use Inc\DefaultData\ElasticsSearch;
use Inc\DefaultData\Translation;

class Activate
{


    public static function activate()
    {
        flush_rewrite_rules();
        self::initial_data();
    } //end activate()


    public static function initial_data()
    {

        $defaultElasticsSearch = get_option('oersi_plugin_elasticsSearch', [
            'url'         => '',
            'app_name'    => '',
            'credentials' => '',
            'fields'      => '',
        ]);

        $defaultTranslation = get_option('oersi_plugin_translation', ['translations' => '']);

        // Check if we need to update the elastics search.
        if (count(array_filter($defaultElasticsSearch)) == 0 || count(array_filter($defaultTranslation)) == 0) {
            update_option('oersi_plugin_elasticsSearch', [
                'url'         => ElasticsSearch::$url,
                'app_name'    => ElasticsSearch::$appName,
                'credentials' => ElasticsSearch::$credential,
                'fields'      => json_encode(ElasticsSearch::$fields),
            ]);
        }
        // Check if we need to update the translation.
        if (count(array_filter($defaultTranslation)) == 0) {
            update_option('oersi_plugin_translation', [
                'translations' => json_encode(Translation::$translations),
            ]);
        }
    }


}//end class
