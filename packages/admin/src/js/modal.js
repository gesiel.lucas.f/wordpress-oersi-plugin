window.addEventListener('load', function () {
	const triggers = document.querySelectorAll('[data-model-trigger]');
	const dialogs = document.querySelectorAll('dialog');

	triggers.forEach(function (el) {
		el.addEventListener('click', () => {
			const getTarget = el.getAttribute('data-target');
			const target = document.querySelector(`[data-name="${getTarget}"]`);
			if (target.hasAttribute('open')) {
				target.close();
			} else {
				target.showModal();
			}
		});
	});

	/* Check for click in backdrop */
	dialogs.forEach(function (el) {
		el.addEventListener('click', ({ target: dialog }) => {
			if (dialog.nodeName === 'DIALOG') {
				dialog.close('dismiss');
			}
		});
	});
});

// example of using the modal
/* <div>
    <button class="button button-primary" data-target="my-dialog" data-model-trigger>
        open modal
    </button>
    <dialog class="oersi-modal-dialog" data-name="my-dialog">
        <header>
            <h2>Title</h2>
            <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </button>
        </header>
        <main>
            This can be a form to edit the translation object.
            <p>example of a paragraph</p>
            <p>example of a paragraph</p>
            <a href="#"> example of a link</a>
            <h1>example of a heading</h1>
            <h2>example of a heading</h2>
            <h3>example of a heading example of a headingexample of a headingexample of a headingexample of a heading</h3>
        </main>
        <footer>
            <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </button>
        </footer>
    </dialog>
</div> */
