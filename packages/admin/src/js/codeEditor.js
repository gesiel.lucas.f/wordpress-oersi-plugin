import * as ace from 'brace';
import 'brace/mode/javascript';
import 'brace/mode/json';
import 'brace/mode/css';
import 'brace/theme/chrome';
import 'brace/theme/solarized_light';
import 'brace/theme/solarized_dark';
// eslint-disable-next-line camelcase
import { css_beautify, js_beautify } from 'js-beautify';

class Editor {
	#editor;
	#theme;
	#language;
	#value;
	#element;
	#option = {
		autoScrollEditorIntoView: true,
		maxLines: 50,
		minLines: 10
	};

	setTheme(theme) {
		this.#theme = `ace/theme/${theme !== undefined ? theme : 'chrome'}`;
		return this;
	}

	setLanguage(language) {
		this.#language = `ace/mode/${language !== undefined ? language : 'javascript'}`;
		return this;
	}

	get value() {
		return this.#editor.getValue();
	}

	setValue(value) {
		this.#value = value;
		return this;
	}

	setElement(element) {
		this.#element = element !== undefined ? element : document.getElementById('editor');
		return this;
	}

	onChange(fn) {
		this.#editor.on('change', e => {
			fn(e);
		});
		return this;
	}

	onPressEnter(fn) {
		this.#editor.commands.addCommand({
			name: 'onPressEnter',
			bindKey: { win: 'Ctrl-Enter', mac: 'Command-Enter' },
			exec: () => {
				fn(this.#editor.getValue());
			}
		});
		return this;
	}

	show() {
		this.#editor = ace.edit(this.#element);
		this.#editor.setTheme(this.#theme);
		// set option
		this.#editor.setOptions(this.#option);
		if (this.#language === 'ace/mode/javascript') {
			this.#editor.getSession().setMode('ace/mode/javascript');
			this.#editor.setValue(this.formatJson(this.#value));
		} else if (this.isJson(this.#value)) {
			this.#editor.getSession().setMode('ace/mode/json');
			this.#editor.setValue(this.formatJson(this.#value));
		} else {
			this.#editor.getSession().setMode('ace/mode/css');
			this.#editor.setValue(this.formatCss(this.#value));
		}
		return this;
	}

	formatCss(value) {
		return css_beautify(value, {
			indent_size: 4,
			indent_char: ' ',
			indent_with_tabs: false,
			editorconfig: false,
			eol: '\n',
			end_with_newline: false,
			indent_level: 0,
			preserve_newlines: true,
			max_preserve_newlines: 10,
			space_in_paren: false,
			space_in_empty_paren: false,
			jslint_happy: false,
			space_after_anon_function: false,
			space_after_named_function: false,
			brace_style: 'collapse'
		});
	}

	formatJson(value) {
		return js_beautify(value, {
			indent_size: 2,
			indent_char: ' ',
			eol: '\n',
			indent_level: 0
		});
	}

	isJson(value) {
		try {
			JSON.parse(value);
			return true;
		} catch (e) {
			return false;
		}
	}

	setOption(option) {
		this.#option = option;
		return this;
	}

	getOption() {
		return this.#option;
	}

	getEditor() {
		return this.#editor;
	}
}

global.codeEditor = new Editor();
