import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import copy from 'rollup-plugin-copy'
const path = require('path');

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		react(),
		copy({
			targets: [
				{ src: './src/images/**/*', dest: path.join(__dirname, '../../assets/images') },
				{ src: './src/assets/**/*.js', dest: path.join(__dirname, '../../assets/js') },
				{ src: './src/assets/languages/**/*.json', dest: path.join(__dirname, '../../assets/languages') },
			]
		})

	],
	resolve: {
		alias: [
			{
				find: '@components',
				replacement: path.resolve(__dirname, 'src/components'),
			},
			{
				find: '@theme',
				replacement: path.resolve(__dirname, 'src/themes'),
			},
			{
				find: '@utils',
				replacement: path.resolve(__dirname, 'src/utils'),
			},
			{
				find: '@hooks',
				replacement: path.resolve(__dirname, 'src/hooks'),
			},
			{
				find: '@helpers',
				replacement: path.resolve(__dirname, 'src/helpers'),
			},
			{
				find: '@providers',
				replacement: path.resolve(__dirname, 'src/providers'),
			},
			{ find: '@api', replacement: path.resolve(__dirname, 'src/api') },
			{
				find: '@images',
				replacement: path.resolve(__dirname, 'src/images'),
			},
			{
				find: '@license-icons',
				replacement: path.resolve(__dirname, 'src/license-icons'),
			},
			{
				find: '@assets',
				replacement: path.resolve(__dirname, 'src/assets')
			}
		],
	},
	envPrefix: 'OERSI_',
	envDir: path.join(__dirname, '../../'),
	optimizeDeps: {
		exclude: ['*.html'],
	},
	build: {
		rollupOptions: {
			output: {
				dir: path.join(__dirname, '../../assets/'),
				entryFileNames: 'js/oer-frontend.js',
				assetFileNames: assetInfo => {
					const ext = assetInfo.name.split('.').at(1);
					if (ext == 'css') return 'css/oer-frontend.css';
					else if (/png|jpe?g|svg|gif|tiff|bmp|ico/i.test(ext))
						return 'images/[name].[ext]';
					else if (ext == 'js') return 'js/oer-frontend.js';
					else if (ext == 'html') return undefined;

					return '[name].[ext]';
				},
			},
		},
		chunkSizeWarningLimit: 2000,
	},
});
