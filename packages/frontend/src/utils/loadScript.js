const loadScript = (FILE_PATH, async = true, type = 'text/javascript') => {
	const URL = window?.OERSI?.assetsUrl || import.meta.env.OERSI_ASSET_URL;

	return new Promise((resolve, reject) => {
		try {
			const scriptEle = document.createElement('script');
			scriptEle.type = type;
			scriptEle.async = async;
			scriptEle.src = URL + FILE_PATH;

			scriptEle.addEventListener('load', ev => {
				resolve({ status: true });
			});

			scriptEle.addEventListener('error', ev => {
				reject({
					status: false,
					message: `Failed to load the script ${URL + FILE_PATH}`,
				});
			});

			document.body.appendChild(scriptEle);
		} catch (error) {
			reject(error);
		}
	});
};

export default loadScript;
