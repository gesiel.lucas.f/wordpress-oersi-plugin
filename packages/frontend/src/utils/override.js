/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

/**
 * @param {number} n - number of characters to be returned example: n = 20
 * @returns {string} - truncated string = string.oersiSubStr(20);
 */
String.prototype.oersiSubStr = function (n = 20) {
	if (n == -1) return this;
	if (this.length > n) return this.substr(0, n) + '...';
	return this;
};

/**
 * Function that will convert php date format to js date format (moment.js)
 * @returns {string} - date format in js
 */
String.prototype.toMoment = function () {
	const conversions = {
		d: 'DD',
		D: 'ddd',
		j: 'D',
		l: 'dddd',
		N: 'E',
		S: 'o',
		w: 'e',
		z: 'DDD',
		W: 'W',
		F: 'MMMM',
		m: 'MM',
		M: 'MMM',
		n: 'M',
		t: '',
		L: '',
		o: 'YYYY',
		Y: 'YYYY',
		y: 'YY',
		a: 'a',
		A: 'A',
		B: '',
		g: 'h',
		G: 'H',
		h: 'hh',
		H: 'HH',
		i: 'mm',
		s: 'ss',
		u: 'SSS',
		e: 'zz',
		I: '',
		O: '',
		P: '',
		T: '',
		Z: '',
		c: '',
		r: '',
		U: 'X',
	};

	return this.replace(/[A-Za-z]+/g, function (match) {
		return conversions[match] || match;
	});
};
