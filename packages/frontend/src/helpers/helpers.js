import moment from 'moment';
import { orderBy } from 'lodash';
import { isEqual } from '@appbaseio/reactivecore/lib/utils/helper';
import i18next from 'i18next';

export function getThumbnailUrl(resourceId) {
	const publicURL =
		window?.OERSI?.publicURL || import.meta.env.OERSI_PUBLIC_URL;
	const fileId =
		resourceId && resourceId.length > 250
			? resourceId.substring(0, 250)
			: resourceId;
	return publicURL + '/thumbnail/' + fileId + '.webp';
}

/**
 * Retrieve the (translated) label for the given component.
 */
export function getLabelForStandardComponent(label, component, translateFnc = () => { }) {
	if (label === 'N/A') {
		return translateFnc('translation.N/A');
	} else if (component === 'language') {
		return translateFnc('languages.' + label);
	} else if (component === 'license') {
		return getLicenseGroupById(label).toUpperCase();
	}
	return translateFnc('labels.' + label) ?? label;
}

/**
 * Get the group for the given license
 * @param {object} license
 */
export function getLicenseGroup(license) {
	if (license && license?.id) {
		return getLicenseGroupById(license?.id);
	}
	return '';
}

/**
 * Get the group for the given license
 * @param {string} licenseId
 */
export function getLicenseGroupById(licenseId) {
	if (licenseId) {
		if (
			licenseId
				.toLowerCase()
				.startsWith('https://creativecommons.org/publicdomain/mark')
		) {
			return 'PDM';
		} else if (licenseId.match('^https?://www.apache.org/licenses/.*')) {
			return 'Apache';
		} else if (
			licenseId.match('^https?://opensource.org/licenses/0?BSD.*')
		) {
			return 'BSD';
		} else if (
			licenseId.match('^https?://www.gnu.org/licenses/[al]?gpl.*')
		) {
			return 'GPL';
		} else if (licenseId.match('^https?://opensource.org/licenses/MIT')) {
			return 'MIT';
		}
		const regex =
			/^https?:\/\/[a-zA-Z0-9.-]+\/(?:licenses|licences|publicdomain)(?:\/publicdomain)?\/([a-zA-Z-]+)/g;
		const match = regex.exec(licenseId);
		if (match) {
			return match[1];
		}
	}
	return '';
}
export function getLicenseLabel(license) {
	let regex =
		/^https?:\/\/creativecommons.org\/(?:licenses|licences|publicdomain)(?:\/publicdomain)?\/([a-zA-Z-]+)(?:\/([0-9.]+))?(?:\/([a-z]{2})(?:$|\/))?/g;
	let match = regex.exec(license);
	if (match) {
		let label;
		const group = match[1].toLowerCase();
		const version = match[2];
		const country = match[3];
		if (group === 'mark') {
			label = 'Public Domain Mark';
		} else if (group === 'zero') {
			label = 'CC0';
		} else {
			label = 'CC ' + group.toUpperCase();
		}
		if (version) {
			label += ' ' + version;
		}
		if (country) {
			label += ' ' + country.toUpperCase();
		}
		return label;
	}
	regex = /^https?:\/\/www.apache.org\/licenses\/LICENSE-([0-9.]+)/g;
	match = regex.exec(license);
	if (match) {
		const version = match[1];
		return 'Apache ' + version;
	}
	regex = /^https?:\/\/opensource.org\/licenses\/MIT/g;
	match = regex.exec(license);
	if (match) {
		return 'MIT';
	}
	regex = /^https?:\/\/opensource.org\/licenses\/(0?BSD.*)/g;
	match = regex.exec(license);
	if (match) {
		return match[1];
	}
	regex = /^https?:\/\/www.gnu.org\/licenses\/([al]?gpl)(?:-([0-9.]+))?/g;
	match = regex.exec(license);
	if (match) {
		let label = 'GNU ' + match[1].toUpperCase();
		const version = match[2];
		if (version) {
			label += ' ' + version;
		}
		return label;
	}

	return '';
}

/**
 *
 * @param {*} callBackFunction a call back function where we can implement our logic
 */
export async function getRequestWithLanguage(callBackFunction) {
	let language = i18next.language;
	if (
		i18next.language === null ||
		i18next.language === '' ||
		i18next.language === undefined
	) {
		language = i18next.languages[0];
	}
	const response = await callBackFunction(language);
	if (!response) {
		for (const fallbackLanguage of i18next.languages.filter(
			item => item !== i18next.language,
		)) {
			const statusOK = await callBackFunction(fallbackLanguage);
			if (statusOK) break;
		}
	}
}

/**
 * function that check if a string is valid Url or not
 * @param {string} str an Url as string to check if is valid or not
 * @returns {boolean} value, true if is valid
 */
export function isValidURL(str) {
	const pattern = new RegExp('(www.|http://|https://|ftp://)');
	return pattern.test(str);
}

/**
 * function that build a url with a path
 * @param {string} str an path to attach in Url
 * @returns {string} return complete url
 */
export function buildUrl(str) {
	let urlBuild =
		window.location.protocol +
		'//' +
		window.location.host +
		process.env.PUBLIC_URL;
	if (str) {
		urlBuild = urlBuild + '/' + str;
	}
	return new URL(urlBuild);
}

/**
 *
 * @param {String} url  the url to check if is valid or not
 * @returns {URL} return the URL object if is valid or null if is not valid
 */
export function extractUrl(url) {
	try {
		return new URL(url);
	} catch (e) {
		return null;
	}
}

/**
 * Function that determines the privacy-policy-link from the given links matches the given language-code (or fallback-lng)
 * @param {Array} privacyPolicyLinks All link from Configuration
 * @param {String} lang  Language Code from Translate
 */
export function getPrivacyPolicyLinkForLanguage(
	privacyPolicyLinks,
	lang,
	fallBackLang,
) {
	let policyEntry = undefined;
	if (privacyPolicyLinks || privacyPolicyLinks instanceof Array) {
		policyEntry = Array.from(privacyPolicyLinks).filter(
			item => item['language'] === lang && item['path'],
		)[0];
		if (policyEntry === undefined) {
			policyEntry = Array.from(privacyPolicyLinks).filter(
				item => fallBackLang.includes(item['language']) && item['path'],
			)[0];
		}
	}

	if (policyEntry !== undefined)
		return !isValidURL(policyEntry['path'])
			? buildUrl(policyEntry['path'])
			: policyEntry['path'];

	return undefined;
}

/**
 * Access a field of the given array and join the values. The values can also be translated.
 * @param {array} array to process
 * @param {fieldAccessor} method that receives an item of the array and should return the field value
 * @param {fieldTranslation} optional, translation-function that translates the field-value
 * @param {onlyFirstItem} optional, if true, only the first item of the array is processed
 * @returns {string} joined values of the array items, separated by a comma or empty string if the array is empty
 */
export function joinArrayField(array, fieldAccessor, fieldTranslation, onlyFirstItem = false) {
	if (array) {
		const filteredArray = array.filter(item => fieldAccessor(item));
		const fields = filteredArray.map(item =>
			fieldTranslation
				? fieldTranslation(fieldAccessor(item))
				: fieldAccessor(item),
		);

		if (onlyFirstItem) {
			return fields.length > 0 ? fields[0] : '';
		}
		return fields.join(', ');
	}
	return '';
}

export function formatDate(date, format) {
	if (date !== null) {
		moment.locale(i18next.language);
		return moment(date).format(format);
	} else {
		return '';
	}
}

export function getSafeUrl(url) {
	const whitelistProtocols = ['http', 'https'];
	return whitelistProtocols.find(
		p => url && url.toString().startsWith(p + ':'),
	)
		? url
		: '';
}

export const customQuery = (value, props) => {
	return value instanceof Array
		? {
			query: {
				bool: {
					should: [
						...value.map(v => ({
							prefix: {
								'license.id': v,
							},
						})),
						...value.map(v => ({
							prefix: {
								'license.id': v.replace(
									'https:/',
									'http:/',
								),
							},
						})),
					],
				},
			},
		}
		: {};
};

export function getPrefixAggregationQuery() {
	const prefixList = [
		'https://creativecommons.org/licenses/by/',
		'https://creativecommons.org/licenses/by-sa/',
		'https://creativecommons.org/licenses/by-nd/',
		'https://creativecommons.org/licenses/by-nc-sa/',
		'https://creativecommons.org/licenses/by-nc/',
		'https://creativecommons.org/licenses/by-nc-nd/',
		'https://creativecommons.org/publicdomain/zero/',
		'https://creativecommons.org/publicdomain/mark',
	];
	const fieldName = 'license.id';
	let aggsScript = "if (doc['" + fieldName + "'].size()==0) { return null }";
	aggsScript += prefixList.reduce(
		(result, prefix) =>
			result +
			" else if (doc['" +
			fieldName +
			"'].value.startsWith('" +
			prefix +
			"') || doc['" +
			fieldName +
			"'].value.startsWith('" +
			prefix.replace('https:/', 'http:/') +
			"')) { return '" +
			prefix +
			"'}",
		'',
	);
	aggsScript += " else { return doc['" + fieldName + "'] }";
	return () => ({
		aggs: {
			'license.id': {
				terms: {
					size: 100,
					script: {
						source: aggsScript,
						lang: 'painless',
					},
				},
			},
		},
	});
}

/**
 * Function that check if a specific license is in the list of licenses
 * @param {Object} license the license object to check
 * @returns {boolean} true if the license is in the list, false otherwise
 **/
export const hasLicenseIcon = license => {
	if (!license || license?.id) return false;

	const licenseGroup = getLicenseGroup(license);
	return (
		licenseGroup !== '' &&
		[
			'by',
			'by-nc',
			'by-nc-nd',
			'by-nc-sa',
			'by-nd',
			'by-sa',
			'pdm',
			'zero',
		].includes(licenseGroup.toLowerCase())
	);
};

/**
 *
 * @param {Array} data  - array of objects with key and value properties
 * @param {Array} fields  - array of fields to be sorted by (key)
 * @param {Array} order - array of order to be sorted by (asc/desc) (asc/desc)
 * @returns  {Array} - sorted array of objects with key and value properties
 * @example
 * let data = [
 * 	{ key: 'a', value: 'a' },
 * 	{ key: 'b', value: 'b' },
 * 	{ key: 'c', value: 'c' }
 * ];
 * const fields = ['key'];
 * const order = ['asc'];
 * data = sortByFields(data, fields, order);
 *
 * @example
 *  // Order by multiple fields and directions
 *  const fields = ['key', 'value'];
 *  const order = ['asc', 'desc'];
 * data = sortByFields(data, fields, order);
 */
export const sortDataBy = (data, fields = [], order = []) => {
	if (order.length === 0 || fields.length === 0) return data;

	return orderBy(data, fields, order);
};

/**
 *
 * @param {Array} array - array of materials to be processed
 * @returns  {Boolean} - true if at least one material in the array is a object otherwise false
 */
const isArrayOfObjects = array => {
	return array.some(item => typeof item === 'object');
};

/**
 *
 * @param {*} prevProps
 * @param {*} nextProps
 * @returns
 */
export function areEqual(prevProps, nextProps) {
	return isEqual(prevProps, nextProps);
}

/**
 * function to get the location and return a value for  specific query parameters
 * @param {Location} location Get location
 * @param {string} queryToSearch String to check if exist or not in URL example: queryToSearch="pageSize"
 */
export function getParams(location, queryToSearch) {
	const searchParams = new URLSearchParams(location.search);
	if (searchParams.has(queryToSearch) === true)
		return searchParams.get(queryToSearch);
	else return null;
}

/**
 *
 * @param {Location} location Get location
 * @param {Object} queryToInsertUpdate Contain key/value for setting or Ubdating Params in URL example {name:'size',value:5}
 */
export function setParams(location, queryToInsertUpdate) {
	const addUpdateParams = new URLSearchParams(location.search);
	addUpdateParams.set(queryToInsertUpdate.name, queryToInsertUpdate.value);
	return addUpdateParams;
}

/**
 * 	Function that get the URL parameter size and return the value or default value if not exist
 * @param {Int} defaultPageSize  Default page size for pagination
 * @param {Array} pageSizeOption	Array of page size options for pagination
 * @returns  {Int} - return the default page size for pagination
 */

export const determineInitialPageSize = (defaultPageSize, pageSizeOption) => {
	const sizeParam = getParams(location, 'size');
	if (
		sizeParam != null &&
		pageSizeOption.includes(parseInt(sizeParam)) === true
	) {
		return parseInt(sizeParam);
	} else {
		return defaultPageSize;
	}
};

/**
 * Checks if the material is from the specified provider
 * @param {String} material - Material to be checked
 * @param {String} providerName - Name of the provider to check against
 * @returns {Object} - An object with 'isFound' boolean indicating if the provider was found and 'index' number indicating the index of the provider
 */
export const checkProviderForMaterial = (material, providerName) => {
	const providerIndex = material?.mainEntityOfPage?.findIndex(page =>
		page.provider?.name?.toLowerCase().trim() === providerName?.toLowerCase()?.trim()
	) ?? -1;

	return {
		found: providerIndex !== -1,
		index: providerIndex
	};
};
