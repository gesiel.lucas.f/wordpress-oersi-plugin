/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { getConfigurationAPI } from '@api';
import { useQuery } from '@tanstack/react-query';

/**
 * A Hook that make a request to the configuration API and returns the configuration.
 * @returns {Object} - The UseQuery result of the configuration query.
 * @example
 * const { data, isLoading, isError } = useConfiguration();
 **/
function useConfiguration() {
	return useQuery(['configuration'], getConfigurationAPI);
}

export default useConfiguration;
