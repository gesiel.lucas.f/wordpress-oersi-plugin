/**
 *
 * @param {Object} props - props to be passed to the component
 * @returns  {ReactElement} - React element containing the icon
 */
import React from 'react';
import CreativeCommonsIcon from './CreativeCommonsIcon';
import LicenseByIcon from './LicenseByIcon';
import LicenseNcIcon from './LicenseNcIcon ';
import LicenseNdIcon from './LicenseNdIcon';

/**
 *
 * @param {Object} props - props to be passed to the component
 * @returns  {ReactElement} - React element containing the icon
 */
const LicenseCcByNcNdIcon = props => {
	return (
		<React.Fragment>
			<CreativeCommonsIcon sx={{ mr: '.25rem' }} {...props} />
			<LicenseByIcon sx={{ mr: '.25rem' }} {...props} />
			<LicenseNcIcon sx={{ mr: '.25rem' }} {...props} />
			<LicenseNdIcon {...props} />
		</React.Fragment>
	);
};

export default LicenseCcByNcNdIcon;
