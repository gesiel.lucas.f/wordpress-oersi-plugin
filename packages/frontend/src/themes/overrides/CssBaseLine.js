// ==============================|| OVERRIDES - ICON BUTTON ||============================== //

export default function CssBaseLine() {
	return {
		'@global': {
			'*::-webkit-scrollbar': {
				width: '0.1em',
			},
			'*::-webkit-scrollbar-track': {
				'-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
			},
			'*::-webkit-scrollbar-thumb': {
				backgroundColor: 'rgba(0,0,0,.1)',
				outline: '1px solid slategrey',
			},
		},
	};
}
