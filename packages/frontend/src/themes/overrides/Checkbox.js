// ==============================|| OVERRIDES - CHECKBOX ||============================== //

export default function Checkbox(theme) {
	return {
		MuiCheckbox: {
			styleOverrides: {
				root: {
					padding: 0,
					color: theme.palette.twilloColor.darkBlue,
					'&.Mui-checked': {
						color: theme.palette.twilloColor.darkBlue,
					},
				},
			},
		},
	};
}
