// ==============================|| OVERRIDES - LINER PROGRESS ||============================== //

export default function Accordion(theme) {
	return {
		MuiAccordion: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.grey[0],
					marginBottom: theme.spacing(2),
					borderRadius: '1em',
					'.Mui-expanded': {
						// maxHeight: "px",
						margin: 0,
						// give border radius left right bottom 0
						borderRadius: '1em 1em 0 0',
						// minHeight: "50px",
					},
				},
			},
		},
		MuiAccordionSummary: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.teal,
					borderRadius: '1em',
					maxHeight: '64px',
					width: '100%',
				},
				expandIconWrapper: {
					color: theme.palette.twilloColor.darkBlue,
				},
			},
		},
		MuiAccordionDetails: {
			styleOverrides: {
				root: {
					maxHeight: '100%',
					maxWidth: '100%',
				},
			},
		},
	};
}
