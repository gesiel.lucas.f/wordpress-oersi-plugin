// ==============================|| OVERRIDES - CHECKBOX ||============================== //

export default function ToolTip(theme) {
	return {
		MuiTooltip: {
			tooltip: {
				fontSize: '2.5rem',
				fontWeight: 'normal',
				fontFamily: 'inherit',
				color: 'inherit',
			},
		},
	};
}
