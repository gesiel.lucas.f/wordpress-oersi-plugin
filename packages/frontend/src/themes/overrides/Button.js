// ==============================|| OVERRIDES - BUTTON ||============================== //

export default function Button(theme, container) {
	const disabledStyle = {
		'&.Mui-disabled': {
			backgroundColor: theme.palette.grey[200],
		},
	};

	return {
		MuiButton: {
			defaultProps: {
				disableElevation: true,
				container: container,
			},
			styleOverrides: {
				root: {
					fontWeight: 400,
				},
				contained: {
					...disabledStyle,
				},
				outlined: {
					...disabledStyle,
				},
				dataSearch: {
					...disabledStyle,
				},
			},
			variants: [
				{
					props: { variant: 'dataSearch', color: 'primary' },
					style: {
						backgroundColor: theme.palette.grey[50],
						color: theme.palette.text.primary,
						'&:hover': {
							backgroundColor: theme.palette.grey[300],
						},
					},
				},
			],
		},
	};
}
