export default function SearchComponent(theme) {
	return {
		searchComponent: {
			appBase: {
				backgroundColor: 'transparent',
				width: '100%',
				boxShadow: 'none',
			},
			dataSearchTitle: {
				display: 'none',
			},
			dataSearchInputGroup: {
				borderColor: 'transparent',
				backgroundColor: 'transparent',
				width: '100%',
				height: '100%',
				borderRadius: '20px 20px 20px 20px',
				fontSize: '1.2rem',
			},
			searchInputGroup: {
				backgroundColor: 'transparent',
				borderRadius: '20px',
				border: '2px solid #0a1f40',
				// height: '3rem',
				'& div:first-of-type': {
					// padding: '4px',
				},
			},
			dataSearchInputAddon: {
				borderLeftColor: 'transparent !important',
				backgroundColor: 'transparent',
				width: '20%',
				padding: '4px',
				'&:last-of-type': {
					borderRadius: '0 20px 20px 0',
				},
				'@media(max-width: 500px)': {
					width: '40%',
				},
			},
			dataSearchInputIcon: {
				color: '#0a1f40',
				width: '100%',
				height: '100%',
			},
			dataSearchInputIconButton: {
				backgroundColor: theme.palette.twilloColor.teal,
				marginLeft: '-23rem',
				width: '25rem',
				marginTop: '1.5px',
				height: '92%',
				fontSize: '1.2rem',
				borderRadius: '20px',
				minWidth: '10px',
				'@media(max-width: 700px)': {
					marginLeft: '-3rem',
					width: '5rem'
				},
			},
			dataSearchListItem: {
				width: '100%',
				// borderRadius: '20px',
				color: 'white',
				fontSize: '1.2rem',
				backgroundColor: 'black',
				'&:hover': {
					backgroundColor: theme.palette.grey[500],
				},
			},
		},
	};
}
