// ==============================|| OVERRIDES - LINER PROGRESS ||============================== //

export default function Card(theme) {
	const cardHeight = '450px';
	return {
		MuiCard: {
			styleOverrides: {
				root: {
					// backgroundColor: theme.palette.twilloColor.white,
					// with: "450px",
					height: '450px',
					position: 'sticky',
					boxShadow: '0em 0em 0.9em rgb(10 31 64 / 50%)',
					'&:hover': {
						boxShadow: '0em 0em 1.2em rgb(10 31 64 / 100%)',
					},
					[theme.breakpoints.down('sm')]: {
						height: '300px'
					},
				},
				media: {
					// backgroundColor: theme.palette.twilloColor.white,
					width: '100%',
					height: '100%',
				},
				content: {
					// backgroundColor: theme.palette.twilloColor.white,
					width: '100%',
					height: '10px',
				},
			},
		},
		MuiCardHeader: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.darkBlue,
					color: theme.palette.grey['A100'],
					width: '100%',
					height: '50px',
					display: 'flex',
					justifyContent: 'center',
					flexDirection: 'row',
				},
				avatar: {
					// marginLeft: "10px",
					// position: "relative",
					alignSelf: 'left',
					textAlign: 'left',
				},
				content: {
					// marginLeft: "10px",
					// position: "relative",
					// top: "10px"
					alignSelf: 'right',
					textAlign: 'right',
				},
			},
		},
		MuiCardContent: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.alabaster,
					width: '100%',
					padding: '3px 10px 3px 10px',
				},
			},
		},
		MuiCardMedia: {
			styleOverrides: {
				root: {
					// height: "50%",
					width: '100%',
					height: '50%',
					objectFit: 'cover',
				},
			},
		},
		MuiCardActions: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.white,
					position: 'sticky',
					bottom: '0',
					width: '100%',
					height: '50px',
				},
			},
		},
	};
}
