// ==============================|| OVERRIDES - CHECKBOX ||============================== //

export default function AppBar(theme) {
	return {
		MuiAppBar: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.grey[0],
				},
			},
		},
	};
}
