// ==============================|| OVERRIDES - CHECKBOX ||============================== //

import { display } from '@mui/system';

export default function Pagination(theme) {
	return {
		MuiPagination: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.alabaster,
					width: '80%',
					maxWidth: '80%',
					fontSize: '1.2rem',
					textAlign: 'center',
				},
				ul: {
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					'& li': {},
				},
				text: {
					fontSize: '1.2rem',
				},
			},
		},
		MuiPaginationItem: {
			styleOverrides: {
				root: {
					backgroundColor: theme.palette.twilloColor.alabaster,
					color: theme.palette.twilloColor.darkBlue,
					fontSize: '1.2rem',
					textAlign: 'center',
					display: 'flex',
					justifyContent: 'center',
					'&:hover': {
						backgroundColor: theme.palette.twilloColor.darkBlue,
						color: theme.palette.twilloColor.alabaster,
					},
					'&:active': {
						backgroundColor: theme.palette.twilloColor.darkBlue,
						color: theme.palette.twilloColor.alabaster,
					},
					'&:focus': {
						backgroundColor: theme.palette.twilloColor.darkBlue,
						color: theme.palette.twilloColor.alabaster,
					},
					'&.Mui-selected': {
						backgroundColor: theme.palette.twilloColor.darkBlue,
						color: theme.palette.twilloColor.alabaster,
					},
				},
			},
		},
	};
}
// ==============================|| OVERRIDES - CHECKBOX ||============================== //
