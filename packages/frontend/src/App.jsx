import { useTheme } from '@mui/material';
import GlobalStyles from '@mui/material/GlobalStyles';
import { ReactiveBase } from '@appbaseio/reactivesearch';
import PropTypes from 'prop-types';
import Layout from '@components/Layout';
import NotFound from '@components/NotFound';
const App = ({ data }) => {
	const theme = useTheme();
	return (
		data &&
		(data?.url && data?.app_name ? (
			<ReactiveBase
				app={data.app_name}
				url={data.url}
				className={theme.classPrefix + 'app-reactive-base'}
			>
				<GlobalStyles
					styles={{
						'*::-webkit-scrollbar': {
							width: '8px',
							height: '5px',
						},
						'*::-webkit-scrollbar:hover': {
							height: '50px',
						},
						'*::-webkit-scrollbar-track-piece': {
							backgroundColor: '#fafafa',
						},
						'*::-webkit-scrollbar-thumb:vertical': {
							height: '50px',
							background:
								'-webkit-gradient(linear, left top, right top, color-stop(0, #ccc), color-stop(100%, #ccc))',
							border: '1px solid #0d0d0d',
							borderTop: '1px solid #666',
							borderLeft: '1px solid #666',
							borderRadius: '50px',
						},
						'*::-webkit-scrollbar-thumb:horizontal': {
							width: '50px',
							background:
								'-webkit-gradient(linear, left top, left bottom, color-stop(0, #ccc), color-stop(100%, #ccc))',
							border: '1px solid #1f1f1f',
							borderTop: '1px solid #666',
							borderLeft: '1px solid #666',
							borderRadius: '50px',
						},
					}}
				/>
				<Layout data={data} />
			</ReactiveBase>
		) : (
			<NotFound error={"Please add the URL and app_name, in admin page/ElasticsSearch"} theme={theme} />
		))
	);
};

App.propTypes = {
	data: PropTypes.object.isRequired,
};

App.defaultProps = {};

export default App;
