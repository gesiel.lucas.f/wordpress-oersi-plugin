import React from 'react';
import PropTypes from 'prop-types';
import { FixedSizeList } from 'react-window';
import {
	Box,
	Checkbox,
	Chip,
	FormControlLabel,
	Grid,
	useTheme,
} from '@mui/material';

const MultiListRender = ({ data, value, onSelectionChange }) => {
	const theme = useTheme();
	const ITEM_SIZE = 40;
	const itemCount = data ? data.length : 0;
	const listHeight = Math.min(250, itemCount * ITEM_SIZE);
	return (
		<Grid item lg={12} md={12} sm={12} xs={12}>
			<FixedSizeList
				height={listHeight}
				itemSize={ITEM_SIZE}
				itemCount={itemCount}
				// width={'100%'}
				// style={{
				// 	minHeight: '50px'
				//  }}
				className={
					theme.classPrefix + 'multi-list-render-fixed-size-list'
				}
			>
				{({ index, style }) => (
					<FormControlLabel
						control={
							<Checkbox
								className={
									theme.classPrefix +
									'multi-list-render-checkbox'
								}
								key={index}
								checked={data[index].key in value}
								onChange={onSelectionChange}
								value={data[index].key}
								style={{ height: ITEM_SIZE + 'px' }}
								sx={{
									// minHeight: 100,
									'& .MuiSvgIcon-root': {
										fontSize: '1.5rem',
									},
								}}
							/>
						}
						label={onItemRender(
							data[index].label,
							data[index].doc_count,
							theme,
						)}
						className={
							'full-width ' +
							theme.classPrefix +
							'multi-list-render-checkbox-full-width'
						}
						sx={{ width: '100%', margin: 0 }}
						style={delete style.width && style}
						classes={{
							label:
								theme.classPrefix +
								'filter-item-label full-width',
						}}
					/>
				)}
			</FixedSizeList>
		</Grid>
	);
};

/**
 *
 * @param {String} label  - label of the item to be rendered
 * @param {String} count - count of the item to be rendered (if any)
 * @param {Object} theme - theme object to be used for rendering
 * @returns  {React.Component} - returns a component with the label and count
 */
const onItemRender = (label, count, theme) => {
	return (
		<Box className={theme.classPrefix + 'filter-item-label'}
		sx={{
			width: '100% !important'
		}}
		>
			<Grid
				container
				spacing={1}
				className={theme.classPrefix + 'filter-item-label-grid'}
				sx={
					{
						// marginTop: '0px',
					}
				}
			>
				<Grid
					item
					xs={8}
					sm={8}
					md={9}
					lg={9}
					className={
						theme.classPrefix + 'filter-item-label-grid-item'
					}
				>
					<Box
						className={theme.classPrefix + 'filter-item-label-text'}
						component='div'
						sx={{
							textOverflow: 'ellipsis',
							overflow: 'hidden',
							whiteSpace: 'nowrap',
						}}
					>
						{label}
					</Box>
				</Grid>
				<Grid
					item
					xs={4}
					sm={4}
					md={3}
					lg={3}
					className={
						theme.classPrefix + 'filter-item-label-grid-item-count'
					}
					sx={{
						position: 'absolute',
						right: '0px',
					}}
				>
					<Chip
						className={
							theme.classPrefix + 'filter-item-label-count-chip'
						}
						component='h3'
						sx={{
							fontSize: '1.0rem',
							marginTop: '5px',
						}}
						label={count}
					/>
				</Grid>
			</Grid>
		</Box>
	);
};

MultiListRender.propTypes = {
	data: PropTypes.array.isRequired,
	isHierarchicalFilter: PropTypes.bool.isRequired,
	onSelectionChange: PropTypes.func.isRequired,
	value: PropTypes.object.isRequired,
};

MultiListRender.defaultProps = {};

export default MultiListRender;
export { onItemRender };
