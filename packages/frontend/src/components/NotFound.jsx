/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import { styled } from '@mui/material/styles';
import { Box, Button, Container, Typography } from '@mui/material';
import { PropTypes } from 'prop-types';
import { useTranslation } from 'react-i18next';

const ContentStyle = styled('div')(({ theme }) => ({
	maxWidth: 480,
	margin: 'auto',
	minHeight: '100vh',
	display: 'flex',
	justifyContent: 'center',
	flexDirection: 'column',
	padding: theme.spacing(12, 0),
}));

const NotFound = ({ error,theme }) => {
	const { t } = useTranslation();
	return (
		<Container className={theme.classPrefix+'not-found-container'} >
			<ContentStyle sx={{ textAlign: 'center', alignItems: 'center' }} className={theme.classPrefix+'not-found-content-style'} >
				<Typography variant='h3' paragraph  className={theme.classPrefix+'not-found-typography-title'} >
					{t('translation.NOT_FOUND_TITLE')}
				</Typography>

				<Typography sx={{ color: 'text.secondary' }} className={theme.classPrefix+'not-found-typography-body'} >
					{import.meta.env.MODE === 'development'
						? error
						: t('translation.NOT_FOUND_BODY')}
				</Typography>

				<Box
					component='img'
					src={window?.OERSI?.assetURL + '/images/oer-notFound.png'}
					sx={{ mx: 'auto', maxWidth: '100%', maxHeight: '100%' }}
					className={theme.classPrefix+'not-found-box-image'}
				/>

				<Button
					variant='contained'
					size='large'
					color='primary'
					href={window?.OERSI?.plugin_url}
					target='_blank'
					className={theme.classPrefix+'not-found-botton'}
				>
					{t('translation.NOT_FOUND_LINK')}
				</Button>
			</ContentStyle>
		</Container>
	);
};

NotFound.PropTypes = {
	error: PropTypes.string.Optional,
};
NotFound.defaultProps = {
	error: '',
};

export default NotFound;
