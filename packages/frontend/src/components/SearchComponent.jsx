/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { DataSearch } from '@appbaseio/reactivesearch';
import {
	Alert,
	Box,
	Button,
	List,
	ListItemButton,
	useTheme,
} from '@mui/material';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import React from 'react';
import { css } from '@emotion/core';
import SearchIcon from '@mui/icons-material/Search';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Sanitize from './Sanitize';
import { checkProviderForMaterial } from "@helpers";

const SearchComponent = ({ data, providerName = undefined, providerImageLink = undefined }) => {
	const { components, classPrefix, breakpoints, palette } = useTheme();
	const { t } = useTranslation();
	const backendURL =
		window?.OERSI?.assetURL || import.meta.env.OERSI_ASSET_URL;
	const shortDescription =
		window?.OERSI?.configuration?.searchTextLength || 100;
	const [mediaQuery, setMediaQuery] = React.useState(window.innerWidth);
	const stylesOverride = css`
		& .${classPrefix}search-component-input-title {
			${{ ...components.searchComponent?.dataSearchTitle }}
		}
		& .${classPrefix}search-component-input-group {
			${{ ...components.searchComponent?.dataSearchInputGroup }}
		}
		& .input-addon {
			${{ ...components.searchComponent?.dataSearchInputAddon }}
		}
		& .input-group {
			${{ ...components.searchComponent?.searchInputGroup }}
		}
	`;
	React.useEffect(() => {
		const handleResize = () => {
			setMediaQuery(window.innerWidth);
		};
		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);
	return (
		data && (
			<Box
				className={classPrefix + 'search-component-container'}
				sx={{ width: '100%' }}
			>
				<DataSearch
					css={stylesOverride}
					className={classPrefix + 'search-component-data-search'}
					// title='search'
					componentId={data?.componentId}
					dataField={data?.dataField}
					placeholder={
						data?.placeholder.indexOf('translation.') > -1
							? t(data?.placeholder)
							: data?.placeholder
					}
					autosuggest={data?.autosuggest}
					highlight={data?.highlight}
					fieldWeights={data?.fieldWeights}
					highlightField={data?.highlightField}
					queryFormat={data?.queryFormat}
					fuzziness={data?.fuzziness}
					size={data?.size}
					showIcon={data?.showIcon}
					iconPosition= {data?.iconPosition}
					showFilter={data?.showFilter}
					debounce={data?.debounce}
					filterLabel={data?.filterLabel}
					enablePopularSuggestions={data?.enablePopularSuggestions}
					expandSuggestionsContainer={
						data?.expandSuggestionsContainer
					}
					customHighlight={(props) => ({
						highlight: data?.customHighlight
					})}
					icon={
						data?.showButton && data?.showButton === true ? (
							<div
								className={
									classPrefix + 'search-component-input-icon'
								}
								style={{
									...components.searchComponent
										?.dataSearchInputIcon,
								}}
							>
								<Button
									sx={{
										...components.searchComponent
											?.dataSearchInputIconButton,
										[breakpoints.down(300)]: {
											fontSize: '1.2rem',
										},
									}}
									variant='dataSearch'
									className={
										classPrefix +
										'search-component-custom-icon'
									}
								>
									{mediaQuery > 300 ? (
										data?.translateText?.indexOf(
											'translation.',
										) > -1 ? (
											t(data?.translateText)
										) : (
											data?.translateText
										)
									) : (
										<SearchIcon />
									)}
								</Button>
							</div>
						) : null
					}
					URLParams={data?.URLParams}
					innerClass={{
						input: classPrefix + 'search-component-input-group',
						title: classPrefix + 'search-component-input-title',
						list: classPrefix + 'search-component-input-list',
						'recent-search-icon':
							classPrefix +
							'search-component-input-recent-search-icon',
						'popular-search-icon':
							classPrefix +
							'search-component-input-popular-search-icon',
					}}
					render={({
						loading,
						error,
						data,
						value,
						downshiftProps: {
							isOpen,
							getItemProps,
							highlightedIndex,
							inputValue,
							selectedItem
						},
					}) => {
						if (loading) {
							return (
								<Alert severity='info'> {'Loading...'} </Alert>
							);
						}
						if (error) {
							return (
								<div>
									<Alert severity='error'>
										{'Something went wrong'}
									</Alert>
								</div>
							);
						}
						return isOpen && Boolean(value.length) ? (
							<Box
								sx={{
									height: '400px',
									overflowY: 'auto',
									position: 'absolute',
									backgroundColor: palette.grey[100],
									border: '1px solid',
									borderColor: palette.grey[100],
									width: '100%',
									zIndex: '1',
								}}
								className={classPrefix + 'search-component-box'}
							>
								{data.map((suggestion, index) => (
									<List
										key={index}
										{...getItemProps({
											item: suggestion,
											key: suggestion.source.id,
											style: {
												backgroundColor:
													highlightedIndex === index
														? palette.grey[200]
														: palette.grey[100],
											},
										})}
										sx={{
											...components.searchComponent
												?.dataSearchListItem,
										}}
										className={
											classPrefix +
											'search-component-list-item'
										}
									>
										<ListItemButton
											alignItems='flex-start'
											sx={{
												width: '100%',
												maxWidth: '100%',
											}}
											className={
												classPrefix +
												'search-component-list-item-button'
											}
										>
											<ListItemAvatar
												className={
													classPrefix +
													'search-component-list-item-avatar'
												}
											>
												<Avatar
													className={
														classPrefix +
														'search-component-list-item-avatar-image'
													}
													sx={{
														width: 24,
														height: 24,
													}}
													alt={
														suggestion?.source
															?.mainEntityOfPage?.[0]
															.provider.name
													}
													src={providerName && checkProviderForMaterial(suggestion?.source, providerName)?.found
														? (providerImageLink || (backendURL + '/images/oer-oersi-logo.svg'))
														: backendURL + '/images/oer-oersi-logo.svg'
													}
												/>
											</ListItemAvatar>
											<ListItemText
												primary={
													<Typography
														sx={{
															display: 'inline',
														}}
														component='span'
														variant='body2'
														color='twilloColor.darkBlue'
														className={
															classPrefix +
															'search-component-list-item-text-primary'
														}
													>
														<b>
															<Sanitize>
															{
																suggestion
																?.source
																?.highlight
																?.name ?
																suggestion
																?.source
																?.highlight
																?.name
																:
																suggestion
																	?.source
																	?.name
															}
															</Sanitize>
														</b>
													</Typography>
												}
												secondary={
													<React.Fragment>
														<Typography
															sx={{
																display:
																	'inline',
															}}
															component='span'
															variant='body2'
															color='twilloColor.darkBlue'
															className={
																classPrefix +
																'search-component-list-item-text-secondary'
															}
														>
															<b>
																{
																	suggestion
																		?.source
																		?.mainEntityOfPage?.[0]
																		?.provider
																		?.name
																}
															</b>

															{suggestion?.source
																?.description
																? ' - '
																: ''}
														</Typography>
														<Sanitize>
															{
																suggestion?.source?.highlight?.description
																? suggestion?.source?.highlight?.description?.[0]?.oersiSubStr(
																	shortDescription,
																)
																: suggestion?.source?.description?.oersiSubStr(
																	shortDescription,
																)
															}
														</Sanitize>
													</React.Fragment>
												}
											/>
										</ListItemButton>
									</List>
								))}
							</Box>
						) : null;
					}}
				></DataSearch>
			</Box>
		)
	);
};

SearchComponent.propTypes = {
	data: PropTypes.object.isRequired,
	providerName: PropTypes.string,
	providerImageLink: PropTypes.string
};

SearchComponent.defaultProps = {};

export default SearchComponent;
