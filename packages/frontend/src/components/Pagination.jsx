/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import {
	Box,
	Grid,
	MenuItem,
	Pagination as PaginationComponent,
	PaginationItem,
	Paper,
	Select,
	Typography,
} from '@mui/material';

import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
const Pagination = ({
	page,
	total,
	pageSizeOptions,
	pageSize,
	onChangePage = () => {},
	onChangePageSize,
	theme,
}) => {
	const { t } = useTranslation();
	const pageCount = Math.ceil(total / pageSize);
	const currentRangeStart = Math.min((page - 1) * pageSize + 1, total);
	const currentRangeEnd = Math.min(page * pageSize, total);
	return (
		<Grid
			container
			spacing={2}
			className={theme.classPrefix + 'pagination-grid-container'}
		>
			<Grid
				item
				xs={12}
				className={theme.classPrefix + 'pagination-grid-container-item'}
			>
				<Paper
					className={
						theme.classPrefix +
						'pagination-grid-container-item-paper'
					}
					sx={{
						display: 'flex',
						flexDirection: 'column',
						paddingTop: theme.spacing(1),
						marginTop: theme.spacing(1),
						width: '100%',
						maxWidth: '100%',
						backgroundColor: theme.palette.twilloColor.alabaster,
					}}
				>
					<Box
						sx={{ alignSelf: 'center' }}
						mb={1}
						className={
							theme.classPrefix +
							'pagination-grid-container-item-paper-box'
						}
					>
						<Typography
							className={
								theme.classPrefix +
								'pagination-grid-container-item-paper-box-typography'
							}
							variant='h5'
						>
							{t('translation.SHOW_TOTAL', {
								rangeStart: currentRangeStart,
								rangeEnd: currentRangeEnd,
								total: total,
							})}
						</Typography>
					</Box>
					<Box
						className={
							theme.classPrefix +
							'pagination-grid-container-item-paper-box-first-siblings'
						}
						sx={{
							display: 'flex',
							flexWrap: 'wrap',
							alignSelf: 'center',
							justifyContent: 'center',
							padding: theme.spacing(1),
							width: '100%',
							maxWidth: '100%',
						}}
					>
						<PaginationComponent
							sx={{
								alignSelf: 'center',
								marginX: theme.spacing(1),
							}}
							count={pageCount > 0 ? pageCount : 1}
							page={page}
							onChange={(_event, value) => onChangePage(value)}
							className={
								theme.classPrefix +
								'pagination-grid-container-item-paper-box-first-siblings-pagination-component'
							}
							size='large'
							// variant='outlined'
							shape='circular'
							showFirstButton={
								window?.OERSI?.configuration
									?.showFirstLastButtons || false
							}
							showLastButton={
								window?.OERSI?.configuration
									?.showFirstLastButtons || false
							}
							renderItem={item => (
								<PaginationItem type='last' {...item} />
							)}
						/>
						<Select
							sx={{ alignSelf: 'center' }}
							value={pageSize}
							size='small'
							displayEmpty
							onChange={event =>
								onChangePageSize(event.target.value)
							}
							className={
								theme.classPrefix +
								'pagination-grid-container-item-paper-box-first-siblings-select'
							}
						>
							{pageSizeOptions.map(v => (
								<MenuItem
									key={v}
									value={v}
									className={
										theme.classPrefix +
										'pagination-grid-container-item-paper-box-first-siblings-select-menu-item'
									}
								>
									{t('translation.PAGE_SIZE_SELECTION', {
										size: v,
									})}
								</MenuItem>
							))}
						</Select>
					</Box>
				</Paper>
			</Grid>
		</Grid>
	);
};

Pagination.propTypes = {
	page: PropTypes.number.isRequired,
	total: PropTypes.number.isRequired,
	pageSizeOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
	pageSize: PropTypes.number.isRequired,
	onChangePage: PropTypes.func.isRequired,
	onChangePageSize: PropTypes.func.isRequired,
	theme: PropTypes.object,
};

Pagination.defaultProps = {
	page: 1,
	pageSizeOptions: [12, 24, 36],
	pageSize: 12,
	theme: {},
	onChangePage: () => {},
	onChangePageSize: () => {},
};

export default Pagination;
