/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import { Link, CardMedia as CardMediaMUI } from '@mui/material';
import React from 'react';
import {
	getSafeUrl,
	getThumbnailUrl
} from '@helpers';
import { LazyLoadComponent } from 'react-lazy-load-image-component';
import PropTypes from 'prop-types';

const CardMedia = ({
	data,
	isImageLinked = false,
	classPrefix,
	backendURL,
	thumbnailUrl = getThumbnailUrl(data?._id),
	theme,
}) => {
	return (
		<LazyLoadComponent
			delayTime={100}
			className={theme.classPrefix + 'card-media-lazy-load-component'}
		>
			{isImageLinked && isImageLinked === true ? (
				<Link
					href={getSafeUrl(data?.id)}
					className={theme.classPrefix + 'card-media-link-image'}
				>
					<CardMediaMUI
						href={getSafeUrl(data?.id)}
						sx={{
							height: '50% !important',
							[theme.breakpoints.down('sm')]: {
								height: '40% !important',
							},
						}}
						className={classPrefix + 'card-media-image'}
						component='img'
						title={data?.name}
						image={
							thumbnailUrl ||
							data?.image ||
							backendURL + '/images/oer-help-outline.svg'
						}
						alt={data?.name}
						loading='lazy'
						onError={e => {
							console.error('Error loading image: ', e);
							e.target.onerror = null;
							e.target.src =
								backendURL + '/images/oer-help-outline.svg';
						}}
					/>
				</Link>
			) : (
				<CardMediaMUI
					href={getSafeUrl(data?.id)}
					sx={{
						height: '50%',
						[theme.breakpoints.down('sm')]: {
							height: '40%',
						},
					}}
					className={classPrefix + 'card-card-media-no-link'}
					component='img'
					title={data?.name}
					image={
						data?.image ||
						backendURL + '/images/oer-help-outline.svg'
					}
					alt={data?.name}
					loading='lazy'
					onError={e => {
						console.error('Error loading image: ', e);
						e.target.onerror = null;
						e.target.src =
							backendURL + '/images/oer-help-outline.svg';
					}}
				/>
			)}
		</LazyLoadComponent>
	);
};

CardMedia.propTypes = {
	data: PropTypes.any,
	isImageLinked: PropTypes.bool,
	classPrefix: PropTypes.string,
	backendURL: PropTypes.string,
	theme: PropTypes.object.isRequired,
};

export default CardMedia;
