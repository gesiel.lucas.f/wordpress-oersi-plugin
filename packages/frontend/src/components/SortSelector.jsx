/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import { Grid, Menu, Typography, useTheme } from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const SortSelector = ({ handleChange, selected, sortOptions, showSortByOption = true }) => {
	const { t } = useTranslation();
	const theme = useTheme()
	const [anchorEl, setAnchorEl] = useState(null);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};

	const handleSelect = (sortOption) => () => {
		handleChange(sortOption);
		handleClose();
	};

	return (
		<>
			{sortOptions && sortOptions?.length > 0 ? (
				<Grid item xl={3} lg={3} xs={6}
					md={3}
					sx={{
						textAlign: "right"
					}}
					className={theme.classPrefix + '-sort-options-grid-item'}
				>
					<Button
						sx={{
							backgroundColor: theme.palette.twilloColor.white,
							textTransform: 'none',
						}}
						id={theme.classPrefix + '-sort-options-results'}
						aria-controls={anchorEl ? theme.classPrefix + '-sort-options-results' : undefined}
						aria-haspopup="true"
						aria-expanded={anchorEl ? 'true' : undefined}
						disableElevation
						onClick={handleClick}
						endIcon={<KeyboardArrowDownIcon />}
						className={theme.classPrefix + '-sort-options-grid-item-button'}
					>
						<Typography component="h5"
							className={theme.classPrefix + '-sort-options-grid-item-button-typography'}
						>
							{" "}
							{selected?.label.indexOf('translation.') > -1
								? t(selected?.label)
								: selected?.label}
							&nbsp;&nbsp;
							{showSortByOption && (<b>&#40; {selected?.sortBy} &#41;</b>)}
						</Typography>
					</Button>
					<Menu
						id={theme.classPrefix + '-sort-options-grid-item-menu'}
						className={theme.classPrefix + '-sort-options-grid-item-menu'}
						anchorEl={anchorEl}
						MenuListProps={{
							'aria-labelledby': theme.classPrefix + '-sort-options-results',
						}}
						keepMounted
						open={Boolean(anchorEl)}
						onClose={handleClose}
					>
						{sortOptions?.map((opt) => (
							<MenuItem key={opt?.label} onClick={handleSelect(opt)} disableRipple
								className={theme.classPrefix + '-sort-options-grid-item-menu-item-' + opt?.label}
							>
								{opt?.label.indexOf('translation.') > -1
									? t(opt?.label)
									: opt?.label}
								&nbsp;&nbsp;
								<sub>{showSortByOption && opt?.sortBy}</sub>
							</MenuItem>
						))}
					</Menu>
				</Grid>
			) : null}
		</>
	)


};

SortSelector.propTypes = {
	handleChange: PropTypes.object.isRequired,
	selected: PropTypes.object.isRequired,
	sortOptions: PropTypes.object,
	showSortByOption: PropTypes.string
};

export default SortSelector;
