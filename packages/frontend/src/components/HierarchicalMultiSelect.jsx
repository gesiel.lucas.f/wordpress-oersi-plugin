import React from 'react';
import {
	Box,
	List,
	ListItem,
	IconButton,
	FormControlLabel,
	Checkbox,
	Collapse,
} from '@mui/material';
import { ChevronRight, ExpandLess } from '@mui/icons-material';
import { onItemRender } from './MultiListRender';

const HierarchicalMultiSelect = props => {
	return (
		<Box sx={{ maxHeight: 240, overflowX: 'hidden' }}>
			<HierarchicalMultiSelectRaw {...props} />
		</Box>
	);
};

const HierarchicalMultiSelectRaw = props => {
	return (
		<List component='div'>
			{props?.data.map(d => (
				<HierarchicalMultiSelectionItem
					key={d.key}
					indent={props.indent}
					data={d}
					expanded={d.expanded}
					value={props.value}
					onSelectionChange={props.onSelectionChange}
					onToggleExpandItem={props.onToggleExpandItem}
					theme={props.theme}
				/>
			))}
		</List>
	);
};

const HierarchicalMultiSelectionItem = props => {
	const { data } = props;
	const indent = props.indent ? props.indent : 0;
	const hasChildItems = data.children && data.children.length > 0;
	const theme = props?.theme;
	return (
		<>
			<ListItem key={data.key} sx={{ padding: 0, paddingLeft: indent }}>
				<IconButton
					size='small'
					onClick={() => props.onToggleExpandItem(data.key)}
					sx={{
						visibility: hasChildItems ? 'visible' : 'hidden',
					}}
					aria-label={'Expand ' + data.key + ' children'}
				>
					{props.expanded ? <ExpandLess /> : <ChevronRight />}
				</IconButton>
				<FormControlLabel
					control={
						<Checkbox
							className={
								theme.classPrefix + 'multi-list-render-checkbox'
							}
							checked={data.selected || data.hasSelectedChild}
							color={data.selected ? 'primary' : 'default'}
							onChange={() => props.onSelectionChange(data)}
							value={data.key}
							sx={{
								// minHeight: 100,
								'& .MuiSvgIcon-root': {
									fontSize: '1.5rem',
								},
							}}
						/>
					}
					label={onItemRender(
						data.label ? data.label : data.key,
						data.doc_count,
						props.theme,
					)}
					className={
						'full-width ' +
						theme.classPrefix +
						'multi-list-render-checkbox-full-width'
					}
					sx={{ width: '100%', margin: 0 }}
					componentsProps={{
						typography: {
							sx: {
								display: 'flex',
								alignItems: 'center',
								overflow: 'hidden',
							},
						},
					}}
					classes={{
						label:
							theme.classPrefix + 'filter-item-label full-width',
					}}
				/>
			</ListItem>
			{hasChildItems ? (
				<Collapse in={props.expanded} unmountOnExit>
					<HierarchicalMultiSelectRaw
						{...props}
						data={data.children}
						indent={indent + 2}
					/>
				</Collapse>
			) : (
				''
			)}
		</>
	);
};

export default HierarchicalMultiSelect;
