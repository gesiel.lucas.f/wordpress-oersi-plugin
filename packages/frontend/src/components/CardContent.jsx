/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import { CardContent as MuiCardContent, Divider, Grid } from '@mui/material';

import { useSpring, useElementScroll, motion } from 'framer-motion';
import Content from '@components/Content';
import { joinArrayField } from '@helpers';
const CardContent = ({ data, t, theme }) => {
	const ref = React.useRef(null);
	const { scrollYProgress } = useElementScroll(ref);
	const scaleX = useSpring(scrollYProgress, {
		stiffness: 100,
		damping: 30,
		restDelta: 0.001,
		duration: 0.5, // seconds to complete the motion
	});

	return (
		<MuiCardContent
			className={theme.classPrefix + 'card-content'}
			sx={{
				height: '400px',
				// overflowY: 'scroll',
				transition: 'all 0.5s ease',
				'&:hover': {
					transform: 'translateY(-280px)',
					// overflowY: 'scroll',
				},
				[theme.breakpoints.down('sm')]: {
					// height: '300px',
					'&:hover': {
						transform: 'translateY(-170px)',
						// overflowY: 'scroll',
					},
				},
			}}
			// onScroll={onScrollHandler}
			// style={{ boxShadow }}
		>
			{/* <motion.div
				className={
					'progress-bar ' + theme.classPrefix + 'motion-progress-bar'
				}
				style={{ scaleX }}
			/> */}
			{/* This is the first row of the card and must be sticky, so we use Grid */}
			<Grid
				container
				spacing={1}
				className={theme.classPrefix + 'card-content-container'}
				sx={{
					marginTop:'0px'
				}}
			>
				<Grid
					item
					xs={12}
					sm={12}
					md={12}
					lg={12}
					className={
						theme.classPrefix + 'card-content-item-grid-first'
					}
					sx={{
						width: '100%',
						position: 'sticky',
						top: '0px',
						zIndex: '1',
					}}
				>
					{window?.OERSI?.configuration?.showProgressBar &&
						window?.OERSI?.configuration?.showProgressBar ===
							true && (
							<motion.div
								className={
									'progress-bar ' +
									theme.classPrefix +
									'card-content-motion-progress-bar'
								}
								style={Object.assign(
									{
										position: 'absolute',
										top: '0px',
										left: '3px',
										width: '101%',
										height: '5px',
										backgroundColor:
											theme.palette.twilloColor.darkBlue,
										borderRadius: '15px',
										transformOrigin: 'left',
									},
									{ scaleX },
								)}
							/>
						)}
				</Grid>
				<Grid
					item
					xs={12}
					sm={12}
					md={12}
					lg={12}
					className={
						theme.classPrefix + 'card-content-item-grid-second'
					}
					sx={{
						height: '390px',
						overflowY: 'scroll',
						[theme.breakpoints.down('sm')]: {
							height: '240px',
						}
					}}
					ref={ref}
				>
					<Divider />
					{data?.name && (
						<Content
							data={data?.name}
							translation={t('cards.NAME')}
							format={'html'}
							theme={theme}
						/>
					)}
					{data?.description && (
						<Content
							data={data?.description}
							translation={t('cards.DESCRIPTION')}
							format={'html'}
							theme={theme}
						/>
					)}
					{data?.creator && (
						<Content
							data={joinArrayField(
								data.creator,
								item => item.name,
								null,
							)}
							translation={t('cards.AUTHOR')}
							format={'html'}
							theme={theme}
						/>
					)}
					{data?.about &&
						data?.about?.length > 0 && (
							<Content
								data={joinArrayField(
									data?.about,
									item => item.id,
									label => t('labels.' + label),
								)}
								translation={t('cards.SUBJECT')}
								format={'text'}
								theme={theme}
							/>
						)}
					{/* {data?.learningResourceType && (
							<Content
								data={joinArrayField(
									data?.learningResourceType,
									item => item.id,
									label => t('labels.' + label),
								)}
								translation={t('cards.MATERIAL_TYPE')}
							/>
						)} */}
					{/* {data?.license?.id && (
							<Content
								data={
									<License
										license={data?.license}
										sx={{
											color: theme.palette.grey[600],
											backgroundColor: theme.palette.grey[300],
											borderColor: theme.palette.grey[300],
											borderRadius: '10px',
											padding: '5px',
										}}
									/>
								}
								transition={t('cards.LICENSE')}
								isReactElement={true}
							/>
						)} */}
					{data?.dateCreated && (
						<Content
							data={data?.dateCreated}
							translation={t('cards.DATE_PUBLISHED')}
							format={'date'}
							theme={theme}
						/>
					)}
					<Divider />
					{ data?.keywords?.length > 0 && (
						<Content
							data={data?.keywords}
							translation={t('cards.KEYWORDS')}
							format={'array'}
							theme={theme}
						/>
					)}
				</Grid>
			</Grid>
		</MuiCardContent>
	);
};

export default CardContent;
