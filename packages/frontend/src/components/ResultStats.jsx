/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import { StateProvider } from '@appbaseio/reactivesearch';
import { CircularProgress, Fade, Grid, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Loading from './Loading';

const ResultStats = ({ theme, sx }) => {
	const { t } = useTranslation();
	return (
		<StateProvider
			componentIds='results'
			includeKeys={['hits', 'isLoading']}
			strict={false}
			render={({ searchState }) => (
				<Grid item xl={2} lg={2} xs={6}
					md={4} >
					<Typography
						aria-label='total-result'
						className={theme.classPrefix + 'result-stats-total-result'}
						component='div'
						variant='h3'
						sx={{
							fontWeight: 'normal',
							borderRadius: '20px',
							textAlign: 'center',
							padding: '5px',
							...sx,
						}}
						color='textPrimary'
					>
						{searchState.results?.isLoading ? (
							!window?.OERSI?.configuration?.loadingFull ? (
								<Fade
									in={searchState.results?.isLoading}
									className={
										theme.classPrefix + 'result-stats-loading'
									}
									mountOnEnter
									unmountOnExit
								>
									<CircularProgress
										aria-label='loading-progress'
										className={
											theme.classPrefix +
											'result-stats-loading-progress'
										}
										color='inherit'
										size={16}
									/>
								</Fade>
							) : (
								<Loading
									isLoading={searchState.results?.isLoading}
								/>
							)
						) : (
							t('translation.SHOW_RESULT_STATS', {
								total: searchState.results?.hits?.total,
							})
						)}{' '}
					</Typography>
				</Grid>
			)}
		/>
	);
};

ResultStats.propTypes = {
	theme: PropTypes.object.isRequired,
	sx: PropTypes.object,
};

export default ResultStats;
