/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { Grid, useTheme, Typography } from '@mui/material';
import React from 'react';
import { useFetch } from '@hooks';
import Collection from '@components/Collection';
import { getCollectionAPI } from '@api';
import Loading from '@components/Loading';
import NotFound from '@components/NotFound';
import { useTranslation } from 'react-i18next';

const Collections = () => {
	const theme = useTheme();
	const { t } = useTranslation();
	const backendURL =
		window?.OERSI?.assetURL || import.meta.env.OERSI_ASSET_URL;
	const { data, isError, error, isLoading } = useFetch(getCollectionAPI);
	return (
		<>
			<Grid
				item
				xs={12}
				className={theme.classPrefix + 'layout-grid-collections-item'}
			>
				<Grid
					container
					spacing={3}
					className={
						theme.classPrefix + 'layout-grid-collections-container'
					}
				>
					<Grid
						item
						xs={10}
						className={
							theme.classPrefix + 'layout-grid-collections-title'
						}
					>
						<Typography
							variant='h6'
							component='h6'
							textAlign='left'
							className={
								theme.classPrefix +
								'layout-topography-collections-title-text'
							}
							sx={{
								fontWeight: 'bold',
								fontSize: '2.5rem',
							}}
						>
							{t('collections.TITLE', {
								what: data?.pagination?.[0].total,
							})}
						</Typography>
					</Grid>
				</Grid>
			</Grid>
			<Grid
				className={theme.classPrefix + 'app-grid-container-collections'}
				container
				spacing={1}
				direction='row'
				alignItems='flex-start'
				justify='flex-start'
				sx={{
					margin: theme.spacing(1.5),
					maxWidth: '100%',
					transition: 'all 0.9s ease',
					marginTop: '3%',
				}}
			>
				{/* {isError ?  : null} */}
				{/* {isLoading ? <Loading /> : null} */}
				{!isLoading && !isError && data
					? data?.collections?.map(collection => (
						<Grid item key={collection.id} xs={6} sm={3} md={2}>
							<Collection
								key={collection.id}
								collection={collection}
								backendURL={backendURL}
								theme={theme}
							/>
						</Grid>
					))
					: null}
			</Grid>
		</>
	);
};

export default Collections;
