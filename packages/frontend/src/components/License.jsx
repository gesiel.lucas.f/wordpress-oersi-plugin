/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import { getSafeUrl, hasLicenseIcon, getLicenseGroup } from '@helpers/helpers';
import LicenseIcon from '@license-icons';
import { Button, IconButton, useTheme } from '@mui/material';
import propTypes from 'prop-types';
const License = ({ license, ...others }) => {
	if (!license && license?.id) return null;
	const licenseGroup = getLicenseGroup(license);
	const theme = useTheme();
	return licenseGroup || hasLicenseIcon(license) ? (
		<IconButton
			className={theme.classPrefix + 'license-icon-button-action'}
			target='_blank'
			rel='noreferrer'
			href={getSafeUrl(license?.id)}
			aria-label={licenseGroup}
			size='large'
			{...others}
		>
			<LicenseIcon
				licenseGroup={licenseGroup}
				className={
					theme.classPrefix +
					'license-group-license-icon-'+licenseGroup
				}
			/>
		</IconButton>
	) : (
		<Button
			{...others}
			className={theme.classPrefix + 'license-button-action'}
			target='_blank'
			rel='noreferrer'
			href={getSafeUrl(license?.id)}
			aria-label={licenseGroup}
		>
			{licenseGroup}
		</Button>
	);
};

License.propTypes = {
	license: propTypes.any,
};

export default License;
