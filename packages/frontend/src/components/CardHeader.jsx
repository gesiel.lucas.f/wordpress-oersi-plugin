/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { Avatar, CardHeader as CardHeaderMUI, Typography } from '@mui/material';
import React from 'react';
import PropTypes from 'prop-types';

const CardHeader = ({ material, providerName, imgSrc, theme }) => {
	return (
		<CardHeaderMUI
			className={theme.classPrefix + 'card-card-header'}
			avatar={
				<Avatar
					className={theme.classPrefix + 'card-card-header-avatar'}
					sx={{
						backgroundColor: theme => theme.palette.grey['A100'],
						height: '30px',
						width: '30px',
					}}
					src={imgSrc}
					alt={providerName || 'Oersi'}
					onError={e => {
						console.error('Error loading image: ', e);
						e.target.onerror = null;
						e.target.src =
							window?.OERSI?.assetURL + '/images/oer-oersi-logo.svg';
					}}
				/>
			}
			title={
				<Typography
					variant='h3'
					component='div'
					className={theme.classPrefix + 'card-card-header-title'}
					color='twilloColor.white'
					sx={
						{
							// fontSize: '1.0vw',
						}
					}
				>
					{material}
					{'   '}
				</Typography>
			}
		/>
	);
};

CardHeader.propTypes = {
	material: PropTypes.string.isRequired,
	providerName: PropTypes.string.isRequired,
	theme: PropTypes.object.isRequired,
	imgSrc: PropTypes.string
};

export default CardHeader;
