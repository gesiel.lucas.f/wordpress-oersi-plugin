/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { ToggleButton, ToggleButtonGroup, Tooltip } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import SortByAlphaIcon from '@mui/icons-material/SortByAlpha';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import PropTypes from 'prop-types';

const SortMultiList = ({ sortBy, onChangeSortBy, theme }) => {
	const { t } = useTranslation();
	return (
		<div>
			<ToggleButtonGroup
				value={sortBy}
				exclusive
				onChange={(e, value) => {
					// onChangeSortBy(value) change only the value of the sortBy
					onChangeSortBy({
						label: value.label,
						value: value.value === 'asc' ? 'desc' : 'asc',
					});
				}}
				aria-label='text alignment'
				className={theme.classPrefix + 'sort-multi-list'}
			>
				<ToggleButton
					value={{ label: 'doc_count', value: sortBy.value }}
					aria-label='left aligned'
					className={theme.classPrefix + 'sort-multi-list-doc-count'}
				>
					<Tooltip
					className={theme.classPrefix + 'sort-multi-list-tooltip-doc-count'}
						title={t('translation.SORT_BY_COUNTER', {
							what: sortBy.value,
						})}
					>
						<FormatListNumberedIcon className={theme.classPrefix + 'sort-multi-list-doc-count-icon'} />
					</Tooltip>
				</ToggleButton>
				<ToggleButton
					value={{ label: 'label', value: sortBy.value }}
					aria-label='right aligned'
					className={theme.classPrefix + '-sort-multi-list-label'}
				>
					<Tooltip
					className={theme.classPrefix + 'sort-multi-list-tooltip-label'}
						title={t('translation.SORT_BY_LABEL', {
							what: sortBy.value,
						})}
					>
						<SortByAlphaIcon className={theme.classPrefix + 'sort-multi-list-label-icon'}/>
					</Tooltip>
				</ToggleButton>
			</ToggleButtonGroup>
		</div>
	);
};

SortMultiList.propTypes = {
	sortBy: PropTypes.object.isRequired,
	onChangeSortBy: PropTypes.func.isRequired,
};

SortMultiList.defaultProps = {
	sortBy: {
		label: 'doc_count',
		value: 'desc',
	},
	onChangeSortBy: () => {},
};

export default SortMultiList;
