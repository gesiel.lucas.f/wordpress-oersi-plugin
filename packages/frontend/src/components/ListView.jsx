/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Hunar Karim <Hunar.Karim@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React, { useState } from 'react';
import {
	Card,
	CardMedia,
	useTheme,
	Typography,
	Box,
	Link,
	Button,
	CardContent
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import { LazyLoadComponent } from 'react-lazy-load-image-component';
import { getSafeUrl, joinArrayField, checkProviderForMaterial } from '@helpers';
import Sanitize from '@components/Sanitize';
import License from '@components/License';



const ListView = ({ data }) => {
	const { name, image, description, learningResourceType, license, creator, mainEntityOfPage, id, providerName } = data;
	const backendURL =
		window?.OERSI?.assetURL || import.meta.env.OERSI_ASSET_URL;
	const theme = useTheme();
	const { t } = useTranslation();
	const [isExpanded, setIsExpanded] = useState(false);
	return (
		<LazyLoadComponent
			delayTime={100}
			className={theme.classPrefix + 'list-view-lazy-load-component'}
		>
			<br />
			<Card sx={{
				display: 'flex',
				borderRadius: 0,
				backgroundColor: 'transparent',
				height: 'auto',
				boxShadow: 'none',
				[theme.breakpoints.down('sm')]: {
					height: 'auto',
					flexDirection: 'column'
				},
				'&:hover': {
					backgroundColor: 'transparent',
					boxShadow: 'none'
				}
			}}
				className={theme.classPrefix + 'list-view-card-container'}
			>
				<CardMedia
					component="img"
					sx={{
						width: 151,
						bjectFit: 'contain',
						height: 'fit-content !important'
					}}
					image={
						image || backendURL + '/images/oer-help-outline.svg'
					}
					alt={name}
					onError={e => {
						console.error('Error loading image: ', e);
						e.target.onerror = null;
						e.target.src =
							backendURL + '/images/oer-help-outline.svg';
					}}
					className={theme.classPrefix + 'list-view-card-media'}
				/>
				<Box sx={{
					display: 'flex',
					flexDirection: 'column',
					width: '100%'
				}}
					className={theme.classPrefix + 'list-view-card-box-container'}
				>
					<CardContent sx={{
						flex: '1 0 auto',
						backgroundColor: 'transparent'
					}}
						className={theme.classPrefix + 'list-view-card-content'}
					>
						<Link
							href={
								providerName && checkProviderForMaterial(data, providerName)?.found
									? getSafeUrl(data.mainEntityOfPage?.[checkProviderForMaterial(data, providerName)?.index].id)
									: getSafeUrl(id)
							}
							target='_blank'
							className={theme.classPrefix + 'list-view-card-content-link'}
						>
							<Typography component="div" variant="h5" className={theme.classPrefix + 'list-view-card-content-name'}>
								{<Sanitize>{name}</Sanitize>}
							</Typography>
						</Link>
						<Typography variant="h6" color="text.secondary" component="span" sx={{
							fontSize: '15px',
						}}
							className={theme.classPrefix + 'list-view-card-content-creator'}
						>
							{creator &&
								joinArrayField(creator, item => item.name, null)}
						</Typography>
						{description && (
							<>
								<Typography variant='span' component='span'
									className={theme.classPrefix + 'list-view-card-content-description'}
								>
									{isExpanded ? (
										<Sanitize>
											{description?.oersiSubStr(-1)}
										</Sanitize>
									) : (
										<Sanitize>
											{description?.oersiSubStr(150)}
										</Sanitize>
									)}
								</Typography>
								<Typography
									className={theme.classPrefix + 'list-view-card-content-show-more-button'}
								>
									{(description).length > 150 && (
										<Button
											variant='text'
											onClick={() => setIsExpanded(!isExpanded)}
										>
											<Typography sx={{ fontSize: '15px' }}>
												{isExpanded
													? t('translation.SHOW_LESS_TEXT')
													: t('translation.SHOW_MORE_TEXT')
												}
											</Typography>
										</Button>
									)}
								</Typography>
							</>
						)}
					</CardContent>
					<Box sx={{
						display: 'flex',
						alignItems: 'center',
						pl: 1,
						pb: 1
					}}
						className={theme.classPrefix + 'list-view-card-box-footer-container'}
					>
						{license && (
							<License
								license={license}
								sx={{
									marginRight: 2,
									width: 'auto',
									color: theme.palette.twilloColor.darkBlue,
									backgroundColor: '#F5F5F5',
									borderColor: theme.palette.twilloColor.white,
									'&:hover': {
										backgroundColor:
											theme.palette.twilloColor.darkBlue,
										borderColor: theme.palette.twilloColor.darkBlue,
										color: theme.palette.twilloColor.white,
									},
								}}
								className={theme.classPrefix + 'list-view-card-box-footer-container-license'}
							/>
						)}
						{learningResourceType && (
							<Typography variant='div' component='div' sx={{ marginRight: 2 }}
								className={theme.classPrefix + 'list-view-card-box-footer-container-learning-recourse-type'}
							>
								{joinArrayField(
									learningResourceType,
									item => item.id,
									label => t('labels.' + label),
									true
								)}

							</Typography>
						)}
					</Box>
				</Box>
			</Card>
		</LazyLoadComponent >
	);
};

export default ListView;
