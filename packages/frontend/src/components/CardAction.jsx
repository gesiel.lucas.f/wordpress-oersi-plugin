/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import { Button, CardActions, Grid } from '@mui/material';
import React from 'react';
import License from '@components/License';
import LaunchIcon from '@mui/icons-material/Launch';
import PropTypes from 'prop-types';

const CardAction = ({ theme, url, license, t }) => {
	return (
		<CardActions
			className='card-actions'
			sx={{ marginTop: 'auto' }}
			disableSpacing
		>
			<Grid container spacing={1}>
				<Grid
					item
					xs={4}
					sm={4}
					md={4}
					sx={{
						display: 'flex',
						alignItems: 'center',
						justifyContent: 'center',
					}}
				>
					{(license && license?.id) && (
					<License
						license={license}
						sx={{
							color: theme.palette.twilloColor.darkBlue,
							width: '100%',
							height: '100%',
							backgroundColor: theme.palette.twilloColor.white,
							borderColor: theme.palette.twilloColor.white,
							borderRadius: '10px',
							padding: '5px',
							'&:hover': {
								backgroundColor:
									theme.palette.twilloColor.darkBlue,
								borderColor: theme.palette.twilloColor.darkBlue,
								color: theme.palette.twilloColor.white,
							},
						}}
					/>
					)}
				</Grid>
				<Grid
					item
					xs={8}
					sm={8}
					md={8}
					sx={{
						display: 'flex',
						textAlign: 'right',
					}}
				>
					<Button
						sx={{
							width: '100%',
							height: '50px',
							color: theme.palette.twilloColor.darkBlue,
							'&:hover': {
								backgroundColor:
									theme.palette.twilloColor.darkBlue,
								borderColor: theme.palette.twilloColor.darkBlue,
								color: theme.palette.twilloColor.white,
							},
							fontSize: '110%',
						}}
						className='button-details'
						target='_blank'
						href={url}
						key={'button-details' + 'data._id'}
					>
						{t('cards.DETAILS')}
						<LaunchIcon className='button-details-icon' />
					</Button>
				</Grid>
			</Grid>
		</CardActions>
	);
};

CardAction.propTypes = {
	theme: PropTypes.object,
	url: PropTypes.string,
	license: PropTypes.object,
	t: PropTypes.func,
};

export default CardAction;
