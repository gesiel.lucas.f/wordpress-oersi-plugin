/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */
import React, { useState } from 'react';
import {
	Box,
	Grid,
	useTheme,
	CssBaseline,
	Container,
	Button,
	Tooltip
} from '@mui/material';
import SearchComponent from '@components/SearchComponent';
import SelectedFilters from '@components/SelectedFilters';
import MultiList from '@components/MultiList';
import Collections from '@components/Collections';
import { getSafeUrl } from '@helpers';
import { useTranslation } from 'react-i18next';
import ViewModuleIcon from '@mui/icons-material/ViewModule';
import ReactiveList from './ReactiveList';
import ListIcon from '@mui/icons-material/List';
import PropTypes from 'prop-types';

const Layout = ({ data }) => {
	const [viewType, setViewType] = useState(window?.OERSI?.configuration?.viewTypeFrontEnd || 'card');

	const { searchComponent, multiList, resultList } = JSON.parse(
		data?.fields || '{}',
	);
	const theme = useTheme();
	const { t } = useTranslation();
	return (
		<Box
			sx={{ top: 0, right: 0, display: 'flex', width: '100%' }}
			className={theme.classPrefix + 'layout-container'}
		>
			<CssBaseline enableColorScheme />
			<Box
				className={theme.classPrefix + 'layout-box-container'}
				component='main'
				sx={{
					backgroundColor: theme =>
						theme.palette.mode === 'light'
							? theme.palette.grey[100]
							: theme.palette.grey[900],
					flexGrow: 1,
					// height: '100vh',
					overflow: 'auto',
					width: `calc(100% - 10px)`,
				}}
			>
				<Container
					className={theme.classPrefix + 'layout-content-container'}
					sx={{ mt: 4, maxWidth: '100% !important', mb: 4 }}
				>
					<Grid
						container
						spacing={3}
						className={
							theme.classPrefix + 'layout-grid-search-container'
						}
					>
						<Grid
							item
							xs={12}
							className={
								theme.classPrefix + 'layout-grid-search-item'
							}
						>
							<SearchComponent
								data={searchComponent}
								providerName={data?.providerName}
								providerImageLink={data?.providerImageLink || undefined}
							/>
						</Grid>
						<Grid
							item
							xs={12}
							md={3}
							lg={2}
							className={
								theme.classPrefix + 'layout-grid-multilist-item'
							}
						>
							{multiList != null &&
								multiList.map((list, index) => (
									<MultiList multiList={list} key={index} />
								))}
						</Grid>
						{/* sx={{ position: "relative", overflowY: "scroll", height: "900px" }} */}

						<Grid
							className={
								theme.classPrefix +
								'layout-grid-result-list-item'
							}
							item
							xs={12}
							md={9}
							lg={10}
							id='reactive-list-id'
						>
							<SelectedFilters theme={theme} />

							<Grid
								sx={{
									display: 'flex',
									justifyContent: 'right',
								}}
							>
								<Tooltip
									className={theme.classPrefix + 'view-type-list-tooltip'}
									title={t('translation.LIST_VIEW')}
								>
									<Button
										sx={{
											margin: theme.spacing(0.5),
											backgroundColor:
												theme.palette.twilloColor.iceBlue,
											'&:hover': {
												backgroundColor:
													theme.palette.twilloColor.darkBlue,
											},
										}}
										className={theme.classPrefix + 'list-view-button'}
										onClick={() => setViewType('list')}>
										{<ListIcon className={theme.classPrefix + 'list-view-icon'} />}
									</Button>

								</Tooltip>
								<Tooltip
									className={theme.classPrefix + 'view-type-card-tooltip'}
									title={t('translation.CARD_VIEW')}
								>
									<Button
										sx={{
											margin: theme.spacing(0.5),
											backgroundColor:
												theme.palette.twilloColor.iceBlue,
											'&:hover': {
												backgroundColor:
													theme.palette.twilloColor.darkBlue,
											},
										}}
										className={theme.classPrefix + 'card-view-button'}
										onClick={() => setViewType('card')}>
										{<ViewModuleIcon className={theme.classPrefix + 'card-view-icon'} />}
									</Button>
								</Tooltip>
							</Grid>

							<ReactiveList
								viewType={viewType}
								reactiveList={resultList}
								providerName={data?.providerName}
								providerImageLink={data?.providerImageLink || undefined}
							/>
						</Grid>
					</Grid>
					{window?.OERSI?.configuration?.isCollectionActive &&
						window?.OERSI?.configuration?.isCollectionActive ===
						true && (
							<Box
								className={
									theme.classPrefix +
									'layout-box-container-collections'
								}
								sx={{
									marginTop: '10%',
								}}
							>
								<Grid
									item
									xs={12}
									className={
										theme.classPrefix +
										'layout-collection-wrapper'
									}
								>
									<Collections />
								</Grid>
								<Grid
									item
									xs={12}
									md={12}
									lg={12}
									textAlign={'right'}
									marginTop={3}
									className={
										theme.classPrefix +
										'layout-grid-item-button-wrapper'
									}
								>
									<Button
										className={
											theme.classPrefix +
											'layout-collections-button-link'
										}
										href={getSafeUrl(
											`${window?.OERSI?.configuration
												?.collectionUrl ||
											document.location.origin
											}/edu-sharing/components/collections`,
										)}
										variant='h6'
										component='a'
										target='_blank'
										sx={{
											color: '#0A1F40',
											fontSize: '1.0rem',
											marginTop: '1rem',
											backgroundColor: '#ddf0f0',
											underline: 'none',
											borderRadius: '25px',
											padding: '20px 16px ',
											margin: '0px 6px',
											fontWeight: 600,
											textDecoration: 'none',
											'&:hover': {
												backgroundColor:
													theme.palette.twilloColor
														.darkBlue,
												color: theme.palette.twilloColor
													.white,
											},
										}}
									>
										{t('collections.BUTTON')}
									</Button>
								</Grid>
							</Box>
						)}
				</Container>
			</Box>
		</Box>
	);
};

Layout.propTypes = {
	data: PropTypes.string
};

export default Layout;
