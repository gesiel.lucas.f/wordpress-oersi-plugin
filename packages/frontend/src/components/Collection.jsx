/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Typography from '@mui/material/Typography';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import LayersIcon from '@mui/icons-material/Layers';
import { Link, Tooltip } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { getSafeUrl } from '@helpers';
import PropTypes from 'prop-types';

const Collection = ({ collection = {}, theme }) => {
	const { t } = useTranslation();
	return (
		<Card
			sx={{
				// width: "",
				height: "200px",
				backgroundColor: collection?.color || '#fff',
				color: '#fff',
				transition:
					'z-index .2s step-start,transform .2s,box-shadow .2s,-webkit-transform .2s',
				'&:hover': {
					transform: 'translateY(-10px)',
					boxShadow: '0 0 25px rgb(0 0 0 / 60%)',
					zIndex: 10,
				},
			}}
			className={theme.classPrefix + 'collection-card'}
		>
			<Link href={getSafeUrl(collection.link)} target='_blank'>
				<CardMedia
					component='img'
					alt={collection.title}
					height='50%'
					image={collection.preview}
					sx={{
						objectFit: 'cover',
						objectPosition: 'center',
						backgroundColor: '#fff',
						height: "50% !important"
					}}
					href={getSafeUrl(collection?.link)}
					title={collection.title}
					className={theme.classPrefix + 'collection-card-media'}
				/>
			</Link>
			<CardContent
				sx={{
					backgroundColor: collection?.color || '#fff',
				}}
				className={theme.classPrefix + 'collection-card-content'}
			>
				<Typography
					gutterBottom
					variant='h6'
					component='div'
					className={theme.classPrefix + 'collection-card-title-h6'}
				>
					{collection.title}
				</Typography>
				<Typography
					variant='body2'
					color='white'
					className={
						theme.classPrefix + 'collection-card-description-body'
					}
				>
					{/* {collection.description} */}
				</Typography>
			</CardContent>
			<CardActions
				sx={{
					display: 'flex',
					justifyContent: 'space-between',
					position: 'absolute',
					bottom: 0,
					width: '100%',
					padding: '0.5rem',
					backgroundColor: collection.color || '#fff',
					color: '#fff',
				}}
				className={theme.classPrefix + 'collection-card-actions'}
			>
				<Tooltip
					title={t('collections.SUBCOLLECTION', {
						count: collection.childReferencesCount,
					})}
					className={theme.classPrefix + 'collection-tooltip-content'}
				>
					<Typography
						variant='body2'
						color='white'
						className={
							theme.classPrefix +
							'collection-card-actions-content-count'
						}
					>
						<InsertDriveFileIcon />{' '}
						{collection?.childReferencesCount}
					</Typography>
				</Tooltip>
				<Tooltip
					title={t('collections.MATERIALS', {
						count: collection?.childCollectionsCount,
					})}
					className={
						theme.classPrefix + 'collection-tooltip-subcollection'
					}
				>
					<Typography
						variant='body2'
						color='white'
						className={
							theme.classPrefix + 'collection-typo-subcollection'
						}
					>
						<LayersIcon /> {collection?.childCollectionsCount}
					</Typography>
				</Tooltip>
			</CardActions>
		</Card>
	);
};


Collection.propTypes = {
	collection: PropTypes.object,
	theme: PropTypes.object
};

Collection.defaultProps = {
	collection: {},
	theme: {}
};

export default Collection;
