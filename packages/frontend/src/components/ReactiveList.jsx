/**
 *
 * @package OERSI
 * @license: MIT
 *
 * @author Edmond Kacaj <Edmond.Kacaj@tib.eu>
 *
 * Copyright (c) 2022 TIB <https://www.tib.eu/en>
 */

import React, { useRef, useState } from 'react';
import ListView from './ListView';
import {
	ReactiveList as ReactiveListComponent,
	StateProvider,
} from '@appbaseio/reactivesearch';
import { Grid, useTheme, useMediaQuery } from '@mui/material';
import Card from '@components/Card';
import ResultStats from '@components/ResultStats';
import Pagination from '@components/Pagination';
import { determineInitialPageSize } from '@helpers';
import SortSelector from './SortSelector';

const ReactiveList = ({ reactiveList, viewType, providerName, providerImageLink }) => {
	const { sortOptions, defaultSortOption, showSortByOption, ...reactiveListProp } = reactiveList;
	const defaultValue = sortOptions?.find(({ dataField }) => dataField === defaultSortOption);
	const [sort, setSort] = useState(defaultValue || sortOptions?.[0]);
	const queryParameters = new URLSearchParams(window.location.search)
	const type = queryParameters.get("search")
	const [displaySearch, setDisplaySearch] = useState(false);
	const theme = useTheme();
	const [pageSize, setPageSize] = React.useState(
		determineInitialPageSize(
			window?.OERSI?.configuration?.pageSize || 12,
			window?.OERSI?.configuration?.pageSizeOptions || [12, 24, 48],
		),
	);
	const scrollToSection = useRef(null);
	const matches = useMediaQuery('(max-width: 1024px)');

	const columnsNumber = (number) => {
		if (number > 12 && number < 1) {
			return 12 / 4;
		}
		return 12 / Number(number);
	}
	return (
		reactiveListProp && (
			<>
				{matches && type !== null ? (
					<div style={{ display: 'block' }} ref={scrollToSection} />) : ''
				}

				{matches && displaySearch ? (
					<div style={{ display: 'block' }} ref={scrollToSection} />) : (
					<div style={{ display: 'none' }} />)
				}

				<ReactiveListComponent
					className={theme.classPrefix + 'results-list-component'}
					{...reactiveListProp}
					// URLParams={false}
					size={pageSize}
					react={{ and: reactiveListProp?.and }}
					defaultQuery={() => ({
						...reactiveListProp.defaultQuery,
						sort: [
							sort ? { [sort?.dataField]: { order: sort?.sortBy } } : {}
						]
					})}
					innerClass={{
						resultsInfo:
							theme.classPrefix +
							'results-list-component-inner-class-resultsInfo',
						sortOptions:
							theme.classPrefix +
							'results-list-component-inner-class-sortOptions',
						resultStats:
							theme.classPrefix +
							'results-list-component-inner-class-resultStats',
						noResults:
							theme.classPrefix +
							'results-list-component-inner-class-noResults',
						pagination:
							theme.classPrefix +
							'results-list-component-inner-class-pagination',
						active:
							theme.classPrefix +
							'results-list-component-inner-class-active',
						list:
							theme.classPrefix +
							'results-list-component-inner-class-list',
						poweredBy:
							theme.classPrefix +
							'results-list-component-inner-class-poweredBy',
					}}
					renderPagination={({
						pages,
						currentPage,
						totalPages,
						setPage,
						...props
					}) => (

						<StateProvider
							componentIds={reactiveListProp.componentId}
							includeKeys={['hits']}
							strict={false}
							onChange={() => {
								setDisplaySearch(true);
								scrollToSection.current?.scrollIntoView()
							}}
							render={({ searchState }) =>
								searchState.results?.hits?.total > 0 && (
									<Pagination
										page={currentPage + 1}
										total={
											searchState.results?.hits?.total
												? searchState.results?.hits
													?.total
												: 0
										}
										pageSizeOptions={
											window.OERSI.configuration
												.pageSizeOptions || [
												12, 24, 48, 96,
											]
										}
										pageSize={pageSize}
										onChangePage={page => {
											setPage(page - 1);
										}}
										onChangePageSize={size => {
											setPageSize(parseInt(size));
											// update params
											const params = new URLSearchParams(
												location.search,
											);
											params.set('size', size);
											params.set(
												reactiveListProp.componentId,
												'1',
											);
											if (history.pushState) {
												window.history.pushState(
													{},
													document.title,
													`${location.pathname
													}?${params.toString()}`,
												);
											}
										}}
										theme={theme}
									/>
								)
							}
						/>
					)}
					renderResultStats={data =>
						data && (
							<Grid container sx={{
								justifyContent: "space-between"
							}}>
								<ResultStats
									theme={theme}
									sx={{
										color: theme.palette.twilloColor.darkBlue,
										backgroundColor:
											theme.palette.twilloColor.white,
										padding: theme.spacing(1.2),
										height: '40px',
										textAlign: 'left',
									}}
									className={
										theme.classPrefix +
										'reactive-list-result-stats'
									}
								/>
								<SortSelector handleChange={setSort} selected={sort} sortOptions={sortOptions || []} showSortByOption={showSortByOption} />
							</Grid>
						)

					}
				>
					{({ data }) => (
						<Grid
							container
							direction='row'
							alignItems='flex-start'
							justifyContent={'center'}
							className={
								theme.classPrefix +
								'reactive-list-component-container'
							}
						>
							{viewType === 'card'
								? data.length > 0 && data.map(item => (
									<Grid
										key={item._id}
										item
										xs={9}
										md={6}
										lg={columnsNumber(Number(window?.OERSI?.configuration?.cardNumberOptions) || 4)}
										flexBasis='max-content'
										width={'100%'}
										maxWidth='100%'
										className={
											theme.classPrefix +
											'reactive-card-component-grid-item'
										}
									>
										<Card key={item._id}
											data={item}
											providerName={providerName || undefined}
											providerImageLink={providerImageLink || undefined}
										/>
									</Grid>
								))
								: data.length > 0 && data.map(item => (
									<Grid
										key={item._id}
										item
										xs={12}
										md={12}
										lg={12}
										flexBasis='max-content'
										width={'100%'}
										maxWidth='100%'
										className={
											theme.classPrefix +
											'reactive-list-component-grid-item'
										}
									>
										<ListView key={item._id}
											data={item}
											providerName={providerName || undefined}
											providerImageLink={providerImageLink || undefined}
										/>
									</Grid>
								))}
						</Grid>
					)}
				</ReactiveListComponent>
			</>
		)
	);
};

ReactiveList.propTypes = {
	// reactiveList: propTypes.object.isRequired,
};

export default ReactiveList;
