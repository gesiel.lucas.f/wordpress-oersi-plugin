import http from './base/http-base';

const getMultilistParentData = async (path) => {
	const { data, status } = await http.get(path);
	return { data, status };
};

export default getMultilistParentData;
