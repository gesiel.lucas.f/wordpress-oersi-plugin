import http from './base/http-base';

const getStyleOverrideAPI = async () => {
	const { data } = await http.get('/cssoverride');
	return data;
};

export default getStyleOverrideAPI;
