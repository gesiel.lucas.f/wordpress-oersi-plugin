import http from './base/http-base';

const httpConfig = http;

// httpConfig.interceptors.response.use(
// 	(response) => {
// 		if (response.status === 200) {
// 			window.OERSI = response.data;
// 		}
// 		return response;

// 	}
// 	, (error) => {
// 		return Promise.reject(error);
// 	}
// );

const getCollectionAPI = async () => {
	const { data } = await httpConfig.get('/collections');
	return data;
};

export default getCollectionAPI;
