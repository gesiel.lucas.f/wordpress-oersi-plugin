import http from './base/http-base';

const httpConfig = http;

// httpConfig.interceptors.response.use(
// 	(response) => {
// 		if (response.status === 200) {
// 			window.OERSI = response.data;
// 		}
// 		return response;

// 	}
// 	, (error) => {
// 		return Promise.reject(error);
// 	}
// );

const getConfigurationAPI = async () => {
	const { data } = await httpConfig.get('/configuration');
	return data;
};

export default getConfigurationAPI;
