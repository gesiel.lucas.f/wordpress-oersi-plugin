<div class="wrap">
    <h1><?php $_ENV['OERSI_PLUGIN_NAME']; ?></h1>
    <?php

    use Inc\Pages\Translation\TranslationCallBack;
    use Inc\Base\CustomSection;
    use Inc\DefaultData\Translation;

    settings_errors();
    ?>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-1"><?php esc_html_e('Translation Front-End', 'oersi-domain'); ?></a></li>
        <li><a href="#tab-2"><?php esc_html_e('Documentation', 'oersi-domain'); ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active oersi-ui-tab-pane">
            <form method="post" action="options.php">
                <?php
                settings_fields(TranslationCallBack::$optionGroup);
                CustomSection::do_settings_sections(TranslationCallBack::$optionName);
                ?>
                <div id="editor" data-editor="translation" class="oersi-ui-translation"></div>
                <?php submit_button(); ?>
            </form>
        </div>
        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li><a href="#Introduction" class="nav-link">Introduction</a></li>
                        <li><a href="#StructureOfTranslation" class="nav-link">Structure of translation</a></li>

                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="Introduction">
                        <header>Introduction</header>
                        <p>
                            Translation is the procedure of taking your website's content in its original language, in OERSI, is possible to use multiples.
                            languages depends in wordpress locale, so in order to change the language of OERSI you need to select the language in wordpress setting.
                        </p>
                        <ul>
                            <li>
                                Client-side JavaScript extends the core language by supplying
                                objects to control a browser and its Document Object Model (DOM).
                                For example, client-side extensions allow an application to place
                                elements on an HTML form and respond to user events such as mouse
                                clicks, form input, and page navigation.
                            </li>
                        </ul>
                    </section>
                    <section class="main-section" id="StructureOfTranslation">
                        <header>Introduction</header>
                        <p>
                            In order the translation to work, you need to have a structure like below,
                            Example below show you a structure of english language that is used for translation,
                            <br />
                        <pre>
                            <code>
                            {
                                "en": {
                                    "translations": {
                                        "N/A": "Not defined",
                                        "INPUT_PLACEHOLDER": "Search by {{what}} ",
                                        "CLEAR_ALL": "Clear all",
                                        "SHOW_RESULT_STATS": "{{total}} results found",
                                        "PAGE_SIZE_SELECTION": "{{size}} / page",
                                        "SHOW_TOTAL": "{{rangeStart}}-{{rangeEnd}} of {{total}} items",
                                        "SORT_BY_COUNTER": "Sort by Count {{what}}",
                                        "SORT_BY_LABEL": "Sort by label {{what}}",
                                        "NOT_FOUND_TITLE": "Sorry, page not found!",
                                        "NOT_FOUND_BODY": "Sorry, we couldn’t find the page you’re looking for. Perhaps you’ve mistyped the URL? Be sure to check your spelling.",
                                        "NOT_FOUND_LINK": "go to plugin",
                                        "ABOUT": "Subject",
                                        "LICENSE": "License",
                                        "RESOURCE_TYPE": "Material type",
                                        "AUTHOR": "Author",
                                        "ORGANISATION": "Source",
                                        "LANGUAGE": "Languages",
                                        "PROVIDER": "Provider",
                                        "SEARCH": "Search",
                                        "SEARCH_LABEL": "Search",
                                        "SEARCH_PLACEHOLDER": "Search for OER ...",
                                        "SEARCH_BUTTON_TEXT": "Suche",
                                        "SHOW_MORE_TEXT": "Show more",
                                        "SHOW_LESS_TEXT": "Show less",
                                        "LIST_VIEW": "List View",
                                        "CARD_VIEW": "Card View",
                                        "SORT_OPTION": "Sort by ...",
                                        "SORT_OPTION_LABEL_ABOUT": "About",
                                        "SORT_OPTION_LABEL_MODIFIED": "Modified"
                                    },
                                    "cards": {
                                        "SUBJECT": "Subject :",
                                        "MATERIAL_TYPE": " Material type :",
                                        "DESCRIPTION": "Description",
                                        "NAME": "Name",
                                        "LICENSE": "License :",
                                        "AUTHOR": "Author :",
                                        "DATE_CREATED": "Date Created :",
                                        "DATE_PUBLISHED": "Publishing year :",
                                        "KEYWORDS": "Keywords :",
                                        "DETAILS": "To Material"
                                    },
                                    "collections": {
                                        "TITLE": "Twillo Collection ({{what}})",
                                        "SUBCOLLECTION": "Subcollections",
                                        "MATERIALS": "Content",
                                        "BUTTON": "Show more"
                                    }
                                }
                            }
                            </code>
                        </pre>
                        <br />
                        Example adding more language
                        <br />
                        <pre>
                            <code>
                            {
                                "de":{
                                    "translations":{
                                        .....
                                        .....
                                        .....
                                    },
                                    "cards":{
                                        .....
                                        .....
                                        .....
                                    },
                                    "collections":{
                                        .....
                                        .....
                                        .....
                                    }
                                },
                                "it":{
                                        .....
                                        .....
                                        .....
                                }

                            }
                            </code>
                        </pre>
                        <br />
                        For translation we use JSON format (Key Value),when key is used by application internally, and Value is the text that will be showed for users,
                        Some Values also have some variable for example: <b>{{what}} </b> this variable later will be replace with the value by system.
                        </p>

                        <br />

                        <div>
                            <button class="button button-primary" data-target="my-dialog" data-model-trigger>
                                click here to copy the default translation for English and German
                            </button>
                            <dialog class="oersi-modal-dialog" data-name="my-dialog">
                                <header>
                                    <h2>Default Configuration</h2>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </header>
                                <main>

                                    <h4>Copy</h4> the text below and past to Component field
                        <pre><code>
                              <?php echo json_encode(Translation::$translations, JSON_PRETTY_PRINT);?>
                        </code></pre>
                                </main>
                                <footer>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </footer>
                            </dialog>
                        </div>
                    </section>
                </div>
        </div>

    </div>
</div>
<script>
    window.addEventListener("load", function() {
        const elements_json = document.querySelector("[data-editor='translation']");
        // get text area element by hidden attribute
        const textarea_json = document.getElementById("translation");
        // get value
        var value = textarea_json.value;
        // debugger;
        const jsEditor = window.codeEditor
            .setElement(elements_json)
            .setValue(value !== "" ? codeEditor.formatJson(value) : "{}")
            .setOption({
                autoScrollEditorIntoView: true,
                maxLines: 0,
                minLines: 30,

            }).show().getEditor();

        jsEditor.getSession().on('change', function(e) {
            // disable submit button if not valid json
            if (codeEditor.isJson(jsEditor.getValue())) {
                document.querySelector("[type='submit']").disabled = false;
                textarea_json.value = JSON.stringify(JSON.parse(jsEditor.getValue()));
            } else {
                document.querySelector("[type='submit']").disabled = true;
            }
        });
    });
</script>
