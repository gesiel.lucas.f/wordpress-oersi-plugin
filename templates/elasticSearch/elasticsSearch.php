<div class="wrap">
    <h1><?php $_ENV['OERSI_PLUGIN_NAME']; ?></h1>
    <?php

    use Inc\Api\Callbacks\CssOverrideCallbacks;
    use Inc\Pages\ElasticsSearch\ElasticsSearchConfigCallbacks;
    use Inc\Base\CustomSection;
    use Inc\DefaultData\ElasticsSearch;

    settings_errors(); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-1"><?php esc_html_e('Elasticsearch config', 'oersi-domain'); ?></a></li>
        <li><a href="#tab-2"><?php esc_html_e('Documentation', 'oersi-domain'); ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active oersi-ui-tab-pane">

            <form method="post" action="options.php">
                <?php
                settings_fields(ElasticsSearchConfigCallbacks::$optionGroup);
                CustomSection::do_settings_sections(ElasticsSearchConfigCallbacks::$optionName);
                ?>
                <div id="editor" data-editor="elastics_search" class="oersi-ui-elastics_search_fields"></div>
                <?php submit_button(); ?>
            </form>
            <script>
                window.addEventListener("load", function() {
                    const elements_json = document.querySelector("[data-editor='elastics_search']");
                    // get text area element by hidden attribute
                    const textarea_json = document.getElementById("elastics_search");
                    // get value
                    var value = textarea_json.value;
                    // debugger;
                    const jsEditor = window.codeEditor
                        .setElement(elements_json)
                        .setValue(value !== "" ? codeEditor.formatJson(value) : "{}")
                        .setOption({
                            autoScrollEditorIntoView: true,
                            maxLines: 0,
                            minLines: 30,

                        }).show().getEditor();

                    jsEditor.getSession().on('change', function(e) {
                        // disable submit button if not valid json
                        if (codeEditor.isJson(jsEditor.getValue())) {
                            document.querySelector("[type='submit']").disabled = false;
                            textarea_json.value = JSON.stringify(JSON.parse(jsEditor.getValue()));
                        } else {
                            document.querySelector("[type='submit']").disabled = true;
                        }
                    });
                });
            </script>
        </div>
        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li><a href="#Information" class="nav-link">Information</a></li>
                        <li><a href="#resultList" class="nav-link">ResultList components</a></li>
                        <li><a href="#searchComponent" class="nav-link">SearchComponent components</a></li>
                        <li><a href="#multiList" class="nav-link">MultiList components</a></li>
                        <li><a href="#Oersi" class="nav-link">OERSI (Open Educational Resources Search Index)</a></li>
                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="Information">
                        <header>
                            <h1>Information</h1>
                        </header>
                        <p>
                            <a href="https://opensource.appbase.io/reactive-manual/getting-started/componentsindex.html" target="_blank">ReactiveSearch </a>
                            provides React UI components for Elasticsearch.
                            This document explains the different kinds of components offered by the library and walks throughs scenarios of when to use which component.
                        </p>
                        <p>
                            Not all the Reactivesearch components are available in OERSI. Some of the components are not available in OERSI. so feel free to use the Reactivesearch components that are available in OERSI
                            <br />
                            And if you want to use components that are not available in OERSI, you are free to contribute to the library.
                            <b>Oersi Plugin </b> you can find <a href="<?php echo $_ENV['OERSI_PLUGIN_REPOSITORY_URL']; ?>" target="_blank">here</a>. <br />
                        </p>
                        <hr />
                        <br />
                        <h3>In OERSI we include some of components:</h3>
                        <h5>
                            Every Component has a link to the official documentation <br />
                            and in documentation you can find all the information about the Component and properties, <br />
                        </h5>
                        <ul>
                            <li>
                                <a href="#resultList">ReactiveList components</a> represent all kinds of list related UI components which typically create a term query.
                            </li>
                            <li>
                                <a href="#searchComponent">SearchComponent components</a> represent all kinds of numbers and dates related UI components which typically create a range based query.
                            </li>
                            <li>
                                <a href="#multiList">MultiList components</a> represent searchbar UIs, which typically apply search on full-text data.
                            </li>
                        </ul>
                    </section>
                    <section class="main-section" id="resultList">
                        <header>
                            <h1>ReactiveList Components</h1>
                        </header>
                        <p><a href="https://opensource.appbase.io/reactive-manual/result-components/reactivelist.html" target="_blank">ReactiveList</a> creates a data-driven result list UI component. This list can reactively update itself based on changes in other components or changes in the database itself
                            <br />
                            in OERSI you can use all the props that are available in main ResultList, below we give an example of props that must be implements.
                            <br />
                            <br />
                        <ul>
                            <li>
                                <p><strong>componentId</strong> <code class="gatsby-code-text">String</code>
                                    unique identifier of the component, can be referenced in other components’ <code class="gatsby-code-text">react</code> prop.</p>
                            </li>
                            <li>
                                <p><strong>dataField</strong> <code class="gatsby-code-text">String</code>
                                    data field to be connected to the component’s UI view. It is useful for providing a sorting context.</p>
                            </li>
                            <li>
                                <p><strong>excludeFields</strong> <code class="gatsby-code-text">String Array</code> [optional]
                                    fields to be excluded in search results.</p>
                            </li>
                            <li>
                                <p><strong>includeFields</strong> <code class="gatsby-code-text">String Array</code> [optional]
                                    fields to be included in search results.</p>
                            </li>
                            <li>
                                <p><strong>stream</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    whether to stream new result updates in the UI. Defaults to <code class="gatsby-code-text">false</code>. <code class="gatsby-code-text">stream: true</code> is appended to the streaming hit objects, which can be used to selectively react to streaming changes (eg. showing fade in animation on new streaming hits, Twitter/Facebook like streams, showing the count of new feed items available like <em>2 New Tweets</em>)</p>
                            </li>
                            <li>
                                <p><strong>scrollOnChange</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    Enables you to customise the window scrolling experience on query change. Defaults to <code class="gatsby-code-text">true</code> i.e. The window will scroll to top in case of the query change, which can be triggered by change in pagination, change in filters or search value, etc. When set to <code class="gatsby-code-text">false</code>, scroll position will stay intact.</p>
                            </li>
                            <li>
                                <p><strong>pagination</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    Defaults to <code class="gatsby-code-text">false</code>, When set to <code class="gatsby-code-text">true</code>, a pagination based list view with page numbers will appear.</p>
                            </li>
                            <li>
                                <p><strong>paginationAt</strong> <code class="gatsby-code-text">String</code> [optional]
                                    Determines the position where to show the pagination, only applicable when <strong>pagination</strong> prop is set to <code class="gatsby-code-text">true</code>. Accepts one of <code class="gatsby-code-text">top</code>, <code class="gatsby-code-text">bottom</code> or <code class="gatsby-code-text">both</code> as valid values. Defaults to <code class="gatsby-code-text">bottom</code>.</p>
                            </li>
                            <li>
                                <p><strong>pages</strong> <code class="gatsby-code-text">Number</code> [optional]
                                    number of user selectable pages to be displayed when pagination is enabled. Defaults to 5.</p>
                            </li>
                            <li>
                                <p><strong>sortBy</strong> <code class="gatsby-code-text">String</code> [optional]
                                    sort the results by either <code class="gatsby-code-text">asc</code> or <code class="gatsby-code-text">desc</code> order. It is an alternative to <code class="gatsby-code-text">sortOptions</code>, both can’t be used together.</p>
                            </li>
                            <li>
                                <p><strong>sortOptions</strong> <code class="gatsby-code-text">Object Array</code> [optional]
                                    an alternative to the <code class="gatsby-code-text">sortBy</code> prop, <code class="gatsby-code-text">sortOptions</code> creates a sorting view in the ReactiveList component’s UI. Each array element is an object that takes three keys:</p>
                                <ul>
                                    <li><code class="gatsby-code-text">label</code> - label to be displayed in the UI.It can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.*</code> when * can be anything example: <code class="gatsby-code-text">translation.SORT_LABEL_BY_MODIFIED</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string</li>
                                    <li><code class="gatsby-code-text">dataField</code> - data field to use for applying the sorting criteria on.</li>
                                    <li><code class="gatsby-code-text">sortBy</code> - specified as either <code class="gatsby-code-text">asc</code> or <code class="gatsby-code-text">desc</code>.</li>
                                </ul>
                            </li>
                            <li>
                                <p><strong>defaultSortOption</strong> <code class="gatsby-code-text">String</code> [optional]
                                    accepts the <b>dataField</b> of the desired sort option to set default sort value from given <code class="gatsby-code-text">sortOptions</code> array.</p>
                            </li>
                            <li>
                                <p><strong>showSortByOption</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    show or hide the  shortBy property from the <code class="gatsby-code-text">sortOptions</code> array.
                                    <code class="gatsby-code-text">default/true</code> will show the shortBy option, otherwise it will hide it.
                            </li>
                            <li>
                                <p><strong>size</strong> <code class="gatsby-code-text">Number</code> [optional]
                                    number of results to show per view. Defaults to 10.</p>
                            </li>
                            <li>
                                <p><strong>showLoader</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    defaults to <code class="gatsby-code-text">true</code>, if set to <code class="gatsby-code-text">false</code> then the ReactiveList will not display the default loader.</p>
                            </li>
                            <li>
                                <p><strong>showResultStats</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    whether to show result stats in the form of results found and time taken. Defaults to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>react</strong> <code class="gatsby-code-text">Object</code> [optional]
                                    a dependency object defining how this component should react based on the state changes in the sensor components.</p>
                            </li>
                            <li>
                                <p><strong>URLParams</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    when set adds the current page number to the url. Only works when <code class="gatsby-code-text">pagination</code> is enabled.</p>
                            </li>
                        </ul>
                        <pre>
                            <code>
                            {
                            "resultList": {
                                "componentId": "results",
                                "dataField": "name",
                                "pagination": true,
                                "stream": false,
                                "showLoader": false,
                                "renderError": true,
                                "showResultStats": true,
                                "paginationAt": "bottom",
                                "sortBy": null,
                                "pages": 5,
                                "size": 12,
                                "URLParams": true,
                                "defaultQuery": {
                                "track_total_hits": true
                                },
                                "sortOptions": [{
                                "label": "translation.SORT_OPTION_LABEL_MODIFIED",
                                "dataField": "mainEntityOfPage.dateModified",
                                "sortBy": "desc"
                                }, {
                                "label": "translation.SORT_OPTION_LABEL_MODIFIED",
                                "dataField": "mainEntityOfPage.dateModified",
                                "sortBy": "asc"
                                }],
                                "defaultSortOption": "mainEntityOfPage.dateModified",
                                "showSortByOption": true
                                "showEndPage": true,
                                "and": ["about"],
                            }
                            </code>
                        </pre>
                        .</p>
                    </section>
                    <section class="main-section" id="searchComponent">
                        <header>
                            <h1>SearchComponent Components</h1>
                        </header>
                        <p><a href="https://opensource.appbase.io/reactive-manual/search-components/datasearch.html" target="_blank">SearchComponent</a> is useful for displaying a list of values where multiple items can be selected at a time, and the values are retrieved by a database query on the field specified in the dataField prop.</p>
                        <br />
                        <br />
                        <ul>
                            <li>specify how search suggestions should be filtered using
                                <code class="gatsby-code-text">react</code> prop
                                example:
                                <pre>
                                        <code>
                                        // specify how and which suggestions are filtered using `react` prop.
                                        react={
                                            "and": ["pricingFilter", "dateFilter"],
                                            "or": ["searchFilter"]
                                        }
                                        </code>
                                    </pre>

                            </li>
                            <li>
                                <p><strong>componentId</strong> <code class="gatsby-code-text">String</code>
                                    unique identifier of the component, can be referenced in other components’ <code class="gatsby-code-text">react</code> prop.</p>
                            </li>
                            <li>
                                <p><strong>dataField</strong> <code class="gatsby-code-text">String or Array</code>
                                    database field(s) to be connected to the component’s UI view. DataSearch accepts an Array in addition to String, useful for applying search across multiple fields.</p>
                            </li>
                            <li>
                                <p><strong>nestedField</strong> <code class="gatsby-code-text">String</code> [optional]
                                    use to set the <code class="gatsby-code-text">nested</code> mapping field that allows arrays of objects to be indexed in a way that they can be queried independently of each other. Applicable only when dataField is a part of <code class="gatsby-code-text">nested</code> type.</p>
                            </li>
                            <li>
                                <p><strong>title</strong> <code class="gatsby-code-text">String</code> [optional]
                                    set the title of the component to be shown in the UI.</p>
                            </li>
                            <li>
                                <p><strong>defaultValue</strong> <code class="gatsby-code-text">string</code> [optional]
                                    set the initial search query text on mount.</p>
                            </li>
                            <li>
                                <p><strong>value</strong> <code class="gatsby-code-text">string</code> [optional]
                                    controls the current value of the component. It sets the search query text (on mount and on update). Use this prop in conjunction with <code class="gatsby-code-text">onChange</code> function.</p>
                            </li>
                            <li>
                                <p><strong>fieldWeights</strong> <code class="gatsby-code-text">Array</code> [optional]
                                    set the search weight for the database fields, useful when dataField is an Array of more than one field. This prop accepts an array of numbers. A higher number implies a higher relevance weight for the corresponding field in the search results.</p>
                            </li>
                            <li>
                                <p><strong>placeholder</strong> <code class="gatsby-code-text">String</code> [optional]
                                    set the placeholder text to be shown in the searchbox input field. Defaults to “Search”.
                                    "translation.SEARCH_PLACEHOLDER", // if start with "translation." then will be translated otherwise will be showed as string,
                                </p>
                            </li>
                            <li>
                                <p><strong>showButton</strong> <code class="gatsby-code-text">boolean</code>
                                    show or hide search button. default <b>false</b></p>
                            </li>
                            <li>
                                <p><strong>translateText</strong> <code class="gatsby-code-text">String</code> The text for the button . Can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.SEARCH_BUTTON</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string
                                </p>
                            </li>
                            <li>
                                <p><strong>showIcon</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    whether to display a search or custom icon in the input box. Defaults to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>iconPosition</strong> <code class="gatsby-code-text">String</code> [optional]
                                    sets the position of the search icon. Can be <code class="gatsby-code-text">left</code> or <code class="gatsby-code-text">right</code>. Defaults to <code class="gatsby-code-text">right</code>.</p>
                            </li>
                            <li>
                                <p><strong>showClear</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    show a clear text icon. Defaults to <code class="gatsby-code-text">false</code>.</p>
                            </li>
                            <li>
                                <p><strong>autosuggest</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    set whether the autosuggest functionality should be enabled or disabled. Defaults to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>strictSelection</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    defaults to <code class="gatsby-code-text">false</code>. When set to <code class="gatsby-code-text">true</code> the component will only set its value and fire the query if the value was selected from the suggestion. Otherwise the value will be cleared on selection. This is only relevant with <code class="gatsby-code-text">autosuggest</code>.</p>
                            </li>
                            <li>
                                <p><strong>defaultSuggestions</strong> <code class="gatsby-code-text">Array</code> [optional]
                                    preset search suggestions to be shown on focus when the search box does not have any search query text set. Accepts an array of objects each having a <strong>label</strong> and <strong>value</strong> property. The label can contain String.</p>
                                example
                                <pre>
                                        <code>
                                        defaultSuggestions={[
                                            { label: 'Songwriting', value: 'Songwriting' },
                                            { label: 'Musicians', value: 'Musicians' },
                                        ]}
                                        </code>
                                    </pre>
                            </li>
                            <li>
                                <p><strong>debounce</strong> <code class="gatsby-code-text">Number</code> [optional]
                                    sets the milliseconds to wait before executing the query. Defaults to <code class="gatsby-code-text">0</code>, i.e. no debounce.</p>
                            </li>
                            <li>
                                <p><strong>highlight</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    whether highlighting should be enabled in the returned results.</p>
                            </li>
                            <li>
                                <p><strong>queryFormat</strong> <code class="gatsby-code-text">String</code> [optional]
                                    Sets the query format, can be <strong>or</strong> or <strong>and</strong>. Defaults to <strong>or</strong>.</p>
                                <ul>
                                    <li><strong>or</strong> returns all the results matching <strong>any</strong> of the search query text’s parameters. For example, searching for “bat man” with <strong>or</strong> will return all the results matching either “bat” or “man”.</li>
                                    <li>On the other hand with <strong>and</strong>, only results matching both “bat” and “man” will be returned. It returns the results matching <strong>all</strong> of the search query text’s parameters.</li>
                                </ul>
                            </li>
                            <li>
                                <p><strong>fuzziness</strong> <code class="gatsby-code-text">String or Number</code> [optional]
                                    Sets a maximum edit distance on the search parameters, can be <strong>0</strong>, <strong>1</strong>, <strong>2</strong> or <strong>“AUTO”</strong>. Useful for showing the correct results for an incorrect search parameter by taking the fuzziness into account. For example, with a substitution of one character, <strong>fox</strong> can become <strong>box</strong>. Read more about it in the elastic search <a href="https://www.elastic.co/guide/en/elasticsearch/guide/current/fuzziness.html">docs</a>.</p>
                            </li>
                            <li>
                                <p><strong>showFilter</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    show as filter when a value is selected in a global selected filters view. Defaults to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>searchOperators</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    Defaults to <code class="gatsby-code-text">false</code>, if set to <code class="gatsby-code-text">true</code> than you can use special characters in the search query to enable the advanced search.<br>
                                    Read more about it <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html">here</a>.</p>
                            </li>
                            <li>
                                <p><strong>filterLabel</strong> <code class="gatsby-code-text">String</code> [optional]
                                    An optional label to display for the component in the global selected filters view. This is only applicable if <code class="gatsby-code-text">showFilter</code> is enabled. Default value used here is <code class="gatsby-code-text">componentId</code>.
                                    Can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.SEARCH_LABEL</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string
                                </p>
                            </li>
                            <li>
                                <p><strong>URLParams</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    enable creating a URL query string parameter based on the selected value of the list. This is useful for sharing URLs with the component state. Defaults to <code class="gatsby-code-text">false</code>.</p>
                            </li>
                            <li>
                                <p><strong>customHighlight</strong> <code class="gatsby-code-text">Object</code> [optional]
                                    when highlighting is enabled, this prop allows specifying the fields which should be returned with the matching highlights.
                                    <pre>
                                        <code>
                                        "customHighlight": {
                                            "pre_tags": ["&lt;mark&gt;"],
                                            "post_tags": ["&lt;/mark&gt;"],
                                            "fields": {
                                                "name": {},
                                                "description": {}
                                            },
                                            "number_of_fragments": 0
                                        },
                                        </code>
                                    </pre>
                            </li>
                        </ul>
                        </li>
                        </ul>
                        <h4>example :</h4>
                        <pre>
                            <code>
                            "searchComponent": {
                                "showIcon": false,
                                "showButton": true,
                                "translateText": "Search",
                                "componentId": "search",
                                "queryFormat": "and",
                                "filterLabel": "translation.SEARCH_LABEL",
                                "dataField": ["name", "creator.name", "description", "keywords"],
                                "fieldWeights": [1, 3],
                                "fuzziness": 0,
                                "debounce": 100,
                                "autosuggest": true,
                                "highlight": true,
                                "iconPosition": "right",
                                "placeholder": "translation.SEARCH_PLACEHOLDER",
                                "showFilter": true,
                                "URLParams": true,
                                "customHighlight": {
                                    "pre_tags": ["&lt;mark&gt;"],
                                    "post_tags": ["&lt;/mark&gt;"],
                                    "fields": {
                                        "name": {},
                                        "description": {}
                                    },
                                    "number_of_fragments": 0
                                },
                                "and": ["author", "results"]
                            }
                            </code>
                        </pre>
                    </section>
                    <section class="main-section" id="multiList">
                        <header>
                            <h1>MultiList Components</h1>
                        </header>
                        <p><a href="https://opensource.appbase.io/reactive-manual/list-components/multilist.html" target="_blank">MultiList</a> is useful for displaying a list of values where multiple items can be selected at a time, and the values are retrieved by a database query on the field specified in the dataField prop.</p>
                        <br />
                        <br />
                        <ul>
                            <li>
                                <p><strong>componentId</strong> <code class="gatsby-code-text">String</code>
                                    unique identifier of the component, can be referenced in other components’ <code class="gatsby-code-text">react</code> prop.</p>
                            </li>
                            <li>
                                <p><strong>dataField</strong> <code class="gatsby-code-text">String</code>
                                    data field to be connected to the component’s UI view. This field is used for doing an aggregation and returns the result. We’re using a <code class="gatsby-code-text">.raw</code> multifield here. You can use a field of type <code class="gatsby-code-text">keyword</code> or <code class="gatsby-code-text">not_analyzed</code> depending on your Elasticsearch cluster.</p>
                            </li>
                            <li>
                                <p><strong>nestedField</strong> <code class="gatsby-code-text">String</code> [optional]
                                    use to set the <code class="gatsby-code-text">nested</code> mapping field that allows arrays of objects to be indexed in a way that they can be queried independently of each other. Applicable only when dataField is a part of <code class="gatsby-code-text">nested</code> type.</p>
                            </li>


                            <li>
                                <p><strong>reloadAggregationsOnSearch</strong> <code class="gatsby-code-text">Boolean</code>
                                    use to make <code class="gatsby-code-text">request</code> in server when you search in search input <code class="gatsby-code-text">false</code> deactivated.</p>
                            </li>

                            <li>
                                <p><strong>aggregationSearchDebounce</strong> <code class="gatsby-code-text">Number</code>
                                    milliseconds, used to make <code class="gatsby-code-text">delay</code> in every type.</p>
                            </li>
                            <li>
                                <p><strong>aggregationSearchMinLength</strong> <code class="gatsby-code-text">Number</code>
                                    used to give <code class="gatsby-code-text">min length of chars</code> before the search will happen .</p>
                            </li>

                            <li>
                                <p><strong>title</strong> <code class="gatsby-code-text">String </code> [optional]
                                    title of the component to be shown in the UI. Defaults to no title being shown.
                                    Can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.RESOURCETYPE</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string
                                </p>
                            </li>
                            <li>
                                <p><strong>loader</strong> <code class="gatsby-code-text">String </code> [optional]
                                    to display an optional loader while fetching the options.</p>
                            </li>
                            <li>
                                <p><strong>size</strong> <code class="gatsby-code-text">Number</code> [optional]
                                    number of list items to be displayed. Defaults to showing a <code class="gatsby-code-text">100</code> items. Max value for this prop can be <code class="gatsby-code-text">1000</code>.</p>
                            </li>
                            <li>
                                <p><strong>sortBy</strong> <code class="gatsby-code-text">String</code> [optional]
                                    sort the list items by one of <code class="gatsby-code-text">count</code>, <code class="gatsby-code-text">asc</code>, or <code class="gatsby-code-text">desc</code>. Defaults to <code class="gatsby-code-text">count</code>, which sorts the list by the frequency of count value, most first.</p>
                            </li>
                            <li>
                                <p><strong>defaultValue</strong> <code class="gatsby-code-text">String Array</code> [optional]
                                    select one or more options from the list on mount. Accepts an <code class="gatsby-code-text">Array</code> object containing the items that should be selected.</p>
                            </li>
                            <li>
                                <p><strong>value</strong> <code class="gatsby-code-text">String Array</code> [optional]
                                    controls the current value of the component. It selects the item from the list (on mount and on update). Use this prop in conjunction with <code class="gatsby-code-text">onChange</code> function.</p>
                            </li>
                            <li>
                                <p><strong>queryFormat</strong> <code class="gatsby-code-text">String</code> [optional]
                                    queries the selected items from the list in one of two modes: <code class="gatsby-code-text">or</code>, <code class="gatsby-code-text">and</code>.</p>
                                <ul>
                                    <li>Defaults to <code class="gatsby-code-text">or</code> which queries for results where any of the selected list items are present.</li>
                                    <li>In <code class="gatsby-code-text">and</code> mode, the applied query filters results where all of the selected items are present.</li>
                                </ul>
                            </li>
                            <li>
                                <p><strong>selectAllLabel</strong> <code class="gatsby-code-text">String</code> [optional]
                                    add an extra <code class="gatsby-code-text">Select all</code> item to the list with the provided label string.</p>
                            </li>
                            <li>
                                <p><strong>showCount</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    show a count of the number of occurrences besides each list item. Defaults to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>showMissing</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    defaults to <code class="gatsby-code-text">false</code>. When set to <code class="gatsby-code-text">true</code> it also retrieves the aggregations for missing fields under the label specified by <code class="gatsby-code-text">missingLabel</code>.</p>
                            </li>
                            <li>
                                <p><strong>missingLabel</strong> <code class="gatsby-code-text">String</code> [optional]
                                    defaults to <code class="gatsby-code-text">N/A</code>. Specify a custom label to show when <code class="gatsby-code-text">showMissing</code> is set to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>hierarchical_filter</strong> <code class="gatsby-code-text">Object</code> [optional] give the URL for where to find the hierarchical_filter, see subject as example
                                <pre>
                                  <code>
                                    "hierarchical_filter": {
                                    "schemaPath": "/locales/vocabularies_parents"
                                    },
                                  </code>
                                </pre>
                            </p>
                            </li>
                            <li>
                                <p><strong>showSearch</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    whether to show a searchbox to filter the list items locally. Defaults to true.</p>
                            </li>
                            <li>
                                <p><strong>placeholder</strong> <code class="gatsby-code-text">String</code> [optional]
                                    placeholder to be displayed in the searchbox, only applicable when the <code class="gatsby-code-text">showSearch</code> prop is set to <code class="gatsby-code-text">true</code>. When applicable, the default placeholder value is set to “Search”
                                    Can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.ABOUT</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string.
                                </p>
                            </li>
                            <li>
                                <p><strong>showFilter</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    show as filter when a value is selected in a global selected filters view. Defaults to <code class="gatsby-code-text">true</code>.</p>
                            </li>
                            <li>
                                <p><strong>filterLabel</strong> <code class="gatsby-code-text">String</code> [optional]
                                    An optional label to display for the component in the global selected filters view. This is only applicable if <code class="gatsby-code-text">showFilter</code> is enabled. Default value used here is <code class="gatsby-code-text">componentId</code>.
                                    Can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.ABOUT</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string
                                </p>
                            </li>
                            <li>
                                <p><strong>URLParams</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    enable creating a URL query string parameter based on the selected value of the list. This is useful for sharing URLs with the component state. Defaults to <code class="gatsby-code-text">false</code>.</p>
                            </li>
                            <li>
                                <p><strong>showLoadMore</strong> <code class="gatsby-code-text">Boolean</code> [optional]
                                    defaults to <code class="gatsby-code-text">false</code> and works only with elasticsearch &gt;= 6 since it uses <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-composite-aggregation.html">composite aggregations</a>. This adds a “Load More” button to load the aggs on demand combined with the <code class="gatsby-code-text">size</code> prop. Composite aggregations are in beta and this is an experimental API which might change in a future release.</p>
                                <p><code class="gatsby-code-text">Note</code>: Composite aggregations do not support sorting by <code class="gatsby-code-text">count</code>. Hence with <code class="gatsby-code-text">showLoadMore</code>, you can only sort by: <code class="gatsby-code-text">asc</code> or <code class="gatsby-code-text">desc</code> order. <code class="gatsby-code-text">sortBy</code> prop defaults to <code class="gatsby-code-text">asc</code> when <code class="gatsby-code-text">showLoadMore</code> prop is used.</p>
                            </li>
                            <li>
                                <p><strong>defaultQuery</strong> <code class="gatsby-code-text">Function</code> [optional]
                                    applies a default query to the result component. This query will be run when no other components are being watched (via React prop), as well as in conjunction with the query generated from the React prop. The function should return a query.
                                    <br/>
                                    To add a default query you need to go to oer-elastics.js ==> oersi/assets/js/oer-elastics.js (you need a plugin-editor for this) and add your function there and then the name of the function add to <b>defaultQuery</b>
                                </p>

                            </li>
                            <li>
                                <p><strong>customQuery</strong> <code class="gatsby-code-text">Function</code> [optional]
                                    applies a default query to the result component. This query will be run when no other components are being watched (via React prop), as well as in conjunction with the query generated from the React prop. The function should return a query.
                                    <br/>
                                    To add a custom query you need to go to oer-elastics.js ==> oersi/assets/js/oer-elastics.js (you need a plugin-editor for this) and add your function there and then the name of the function add to <b>defaultQuery</b>
                                </p>

                            </li>
                            <li>
                                <p><strong>tooltipText</strong> <code class="gatsby-code-text">String </code> [optional]
                                tooltip text, if is not empty then it will add an information logo with a tooltip text. Defaults is not showed.
                                    Can be <code class="gatsby-code-text">Text</code> or <code class="gatsby-code-text">translation.TOOLTIPTEXT (or whatever text you want after translation.* )</code>.
                                    if start with "translation." then will be translated otherwise will be showed as string
                                </p>
                            </li>
                        </ul>
                        <pre>
                            <code>
                            "multilist":[{
                                    "componentId": "license",
                                    "dataField": "license.id",
                                    "title": "translation.LICENSE",
                                    "placeholder": "translation.LICENSE",
                                    "filterLabel": "translation.LICENSE",
                                    "showMissing": true,
                                    "missingLabel": "N/A",
                                    "showFilter": true,
                                    "showSearch": false,
                                    "defaultQuery": "getPrefixAggregationQueryLicense",
                                    "customQuery": "customQueryLicense",
                                    "className": "license-card",
                                    "fontAwesome": "",
                                    "URLParams": true,
                                    "hierarchical_filter": {
                                      "schemaPath": "/locales/vocabularies_parents"
                                    },
                                    "and": ["author", "search", "provider", "results", "learningResourceType", "language", "about", "sourceOrganization"]
                                }]
                            </code>
                        </pre>
                    </section>
                    <section class="main-section" id="Oersi">
                        <header>
                            <h1>OERSI - Information</h1>
                        </header>
                        <h4>OERSI (Open Educational Resources Search Index)</h4>
                        <p>OERSI is a plugin that is develop for WordPress, and is a simplified version of <a href="https://oersi.org/resources/" target="_blank">oersi.de</a>.
                            Mostly all the functionality of oersi.de are available in this plugin.
                        </p>


                        <h2>OERSI - Example of Configuration</h2>
                        <p>The following is an example of configuration for the plugin. for more information about the URL, App name, Credentials. please refer to the <a href="https://docs.appbase.io/docs/reactivesearch/v3/overview/reactivebase/#props" target="_blank">ReactiveBase </a> </p>
                        <ol>
                            <li>
                                <h4>URL</h4> <i>Type:</i> string [optional]<br> A field that contains the URL of the Elastic search instance, for example <code>http://localhost:9200</code>.
                            </li>
                            <li>
                                <h4>App Name</h4>  <i>Type:</i> string [optional]<br> Refers to an index if you're using your own Elasticsearch cluster for example <code>oersi_internal</code>. (Multiple indexes can be connected to by specifying comma-separated index names)
                            </li>
                            <li>
                                <h4>Credentials</h4> <i>Type:</i> string [optional]<br>  app credentials as they appear on the dashboard. It should be a string of the format "username:password" and is used for authenticating the app. If you are not using an appbase.io app, credentials may not be necessary - although having an open-access Elasticsearch cluster is not recommended.
                            </li>
                            <li>
                            <h4>Provider Name</h4> <i>Type:</i> string [optional]<br> Accepts a string. When set, it activates provider links. This means that for the specified provider, all links will lead to the provider instead of their actual destinations.
                            </li>
                            <li>
                            <h4>Provider Image Link</h4> <i>Type:</i> string [optional]<br> Accepts an image link (URL). If set, the logo on the cards will be changed from the Oersi logo to the provider logo, but only if the <b>Provider Name</b> is also set.
                            </li>
                        </ol>
                        <div>
                            <button class="button button-primary" data-target="my-dialog" data-model-trigger>
                                click here to copy the default configuration
                            </button>
                            <dialog class="oersi-modal-dialog" data-name="my-dialog">
                                <header>
                                    <h2>Default Configuration</h2>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </header>
                                <main>

                                    <h4>Copy</h4> the text below and past to Component field
                            <pre><code>
                            <?php echo json_encode(ElasticsSearch::$fields, JSON_PRETTY_PRINT);?>
                            </code></pre>
                                </main>
                                <footer>
                                    <button class="btn-oersi btn-oersi-icon" data-target="my-dialog" data-model-trigger>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </footer>
                            </dialog>
                        </div>
                    </section>

                </div>
            </main>
        </div>
    </div>
</div>
