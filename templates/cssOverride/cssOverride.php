<div class="wrap">
    <h1><?php $_ENV['OERSI_PLUGIN_NAME']; ?></h1>
    <?php

    use Inc\Pages\CssOverride\CssOverrideCallbacks;
    use Inc\Base\CustomSection;

    settings_errors(); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-1"><?php esc_html_e('Css overrides', 'oersi-domain'); ?></a></li>
        <li><a href="#tab-2"><?php esc_html_e('Documentation', 'oersi-domain'); ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane tab-pane active oersi-ui-tab-pane">
            <form method="post" action="options.php">
                <?php
                settings_fields(CssOverrideCallbacks::$optionGroup);
                CustomSection::do_settings_sections(CssOverrideCallbacks::$optionName);
                ?>

                <div id="editor" data-editor="css_override" class="oersi-ui-css_override_fields"></div>

                <?php
                submit_button();
                ?>
            </form>
            <script>
                window.addEventListener("load", function() {
                    const elements_css = document.querySelector("[data-editor='css_override']");
                    // get text area element by hidden attribute
                    const textarea_css = document.getElementById("css_override");
                    // get value
                    var value = textarea_css.value;
                    // debugger;
                    const cssEditor = window.codeEditor
                        .setElement(elements_css)
                        .setValue(value !== "" ? codeEditor.formatCss(value) : "")
                        .setOption({
                            autoScrollEditorIntoView: true,
                            maxLines: 0,
                            minLines: 30,

                        }).show().getEditor();

                    cssEditor.getSession().on('change', function(e) {
                        textarea_css.value = cssEditor.getValue();
                    });
                });
            </script>
        </div>

        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li><a href="#About" class="nav-link">Information</a></li>

                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="About">
                        <header>
                            <h1>Information</h1>
                        </header>
                        <p>
                            OERSI provide and default style for search (frontend) part. Some users want to have they own style, own color margin etc, and OERSI give you that opportunity to add you own style, in OERSI we have included some classes that can be override by css in admin panel without needed to rebuild the project again, every element contain clasees that start with <b><?php echo $_ENV['OERSI_PLUGIN_CLASS_PREFIX'] ?>*</b>, those classes remain the same in every feature builds, so you don't need to re-style again because of incompatible
                        </p>
                        <br />
                        <br />
                        <h3>Example of styling </h3>
                        <pre>
                        <code>
                        .<?php echo $_ENV['OERSI_PLUGIN_CLASS_PREFIX'] ?>layout-container{
                            background-color: white;
                            font-size: 14px;
                            ...
                            ...
                            etc
                        }
                        </code>
                        </pre>
                        <hr />
                    </section>
                </div>
            </main>
        </div>
    </div>
