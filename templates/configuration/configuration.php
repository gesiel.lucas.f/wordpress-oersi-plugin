<div class="wrap">
    <h1><?php $_ENV['OERSI_PLUGIN_NAME']; ?></h1>
    <?php

    use Inc\Pages\Configuration\ConfigurationCallbacks;
    use Inc\Base\CustomSection;

    settings_errors(); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-1"><?php esc_html_e('Configuration', 'oersi-domain'); ?></a></li>
        <li><a href="#tab-2"><?php esc_html_e('Documentation', 'oersi-domain'); ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="tab-1" class="tab-pane active oersi-ui-tab-pane">

            <form method="post" action="options.php">
                <?php
                settings_fields(ConfigurationCallbacks::$optionGroup);
                CustomSection::do_settings_sections(ConfigurationCallbacks::$optionName);
                submit_button();
                ?>
            </form>
        </div>
        <div id="tab-2" class="tab-pane">
            <main id="oersi-doc-main-doc">
                <nav id="oersi-doc-navbar">
                    <ul>
                        <li><a href="#About" class="nav-link">Information</a></li>
                        <li><a href="#fieldsDescription" class="nav-link">Fields Descriptions</a></li>

                    </ul>
                </nav>
                <div class="container">
                    <section class="main-section" id="About">
                        <header>
                            <h1>Information</h1>
                        </header>
                        <p>
                            In addition to using the environment variable of your project to configure your project, you can use the admin panel in the project. All configurations here affect the frontend automatically without needed to rebuild the project again.

                        </p>
                        <br />
                        <br />
                        <br />
                        <h3>Activate Search in a wordpress Page</h3>
                        <br />
                        <p>
                            To activate Search (frontend), in any page, si very simple, You just need to use the wordpress <b>short code</b>, and paste the name of search <code>oersi_search</code>, inside the short code input.
                            <br />
                            <br />
                        <h4>Example:</h4>
                        <!-- <pre> -->
                        <code>
                            [oersi_search]
                        </code>
                        <!-- </pre> -->
                        <br />
                        <br />
                        and the Search is activated and ready :),
                        <br />
                        <br />
                        <b>Information!</b> Some of the configuration are required to be set, in order to work the frontend, so please add configuration for each part. In every configuration in documentation in the end you will find  default configuration, so copy and paste them in specific input fields and then refresh the page.
                        </p>
                        <hr />
                    </section>
                    <section class="main-section" id="fieldsDescription">
                        <header>
                            <h1>Fields Descriptions</h1>
                        </header>
                        <p>
                            So what you can configure in this panel, below we will give a description for all the fields
                        </p>
                        <hr />
                        <h2>List of Fields </h2>
                        <ol>
                            <li>
                                <h4>Activate Twillo collection</h4><i>boolean</i> This fields can decide if you want to show or hide the collection in search page example:<code>checked = true (show the collection)</code>
                            <li>
                                <h4>URL of collection</h4> <i>string</i> Gives an Url where you want to get collection, This work only with the edu-sharing instances. Collection are sorted base on <b>date modified</b>, so last modified are showed. example: <code>https://www.twillo.de (Oly the domain must be entered)</code>
                            </li>
                            <li>
                                <h4>Choose the length of collection</h4> <i>number</i> You can decide the number of collections to be showed in frontend panel example: <code>10 (Only 10 collections last modified with be showed)</code>
                            </li>
                            <li>
                                <h4>Choose the default page size</h4> <i>number</i> You can decide the number of cards to be showed in frontend (pages size) example: <code>12 (Only 12 materials (cards) with be showed)</code>.<br />
                                <b>Choose the default page size</b> must correspond with one of the numbers in <b>Choose the page size options</b>
                            </li>
                            <li>
                                <h4>Choose the page size options</h4> <i>string</i> Decide the pages size options to be showed in the list of page size in frontend, Multiple number must separated with <b>. (comma)</b> example: <code>12,24,96 (Bug: Since for now this has a small bag you need to give a minimum of 2 numbers)</code>
                            </li>
                            <li>
                                <h4>Show first and last buttons</h4> <i>boolean</i> Show or hide the first and last button in pagination (frontend), First and Last button give users opportunity to move directly to the end of pagination example: <code>checked= true (Bug: Since we have a large numbers in OERSI, we are not allowed to retrieve more than 10,000 materials, so last page will not work for now)</code>
                            </li>
                            <li>
                                <h4>Choose the show progress bar</h4> <i>boolean</i> In cards we have a possibility to show more information about a material by just moving the mouse (long touching mobile/tablet), in the body of card, and then we can scroll and see all the information, <b>show progress bar</b> give a nice progress bar above the content when we move inside the content example: <code>checked= true (activated)</code>
                            </li>
                            <li>
                                <h4>Select if you want to show the loading full</h4> <i>boolean</i> Loading animation can improve the user experience significantly and helps them understand what is going on is the content take time to load. in OERSI plugin we have 2 kind of loading, <b>full site animation</b> and <b>animation that happen inside the statistics</b> : <code>checked= true (activated full loading )</code>
                            </li>
                            <li>
                                <h4>Select if you want to show the html tags</h4> <i>boolean</i> Some of materials can have <b>html</b> tags in their descriptions, in order to render them rather than printing them as a string. So You can choose to render or to print them as a string : <code>checked= true (Render html tags)</code>
                            </li>
                            <li>
                                <h4>Select length of the text you want to show in search and Selected filters</h4> <i>number</i> Some descriptions have a long text, in order to reduce the length of description for search input and the selected field then you can decide the length of text to be displayed (This will not affect the description in card content) : <code>default will 20</code>
                            </li>
                            <li>
                                <h4>Select the number of cards to be displayed horizontally</h4><i>number</i> You can select only the number (2, 3, 4, 5, and 6) to displayed cards horizontally  : <code>default will 2</code>
                            </li>
                        </ol>
                    </section>
                </div>
            </main>
        </div>
    </div>
</div>
