��          �      �       0     1  )   D     n     �     �     �     �  5   �       
        )     D  �  R       .   $     S     m     �     �     �  ;   �     	  
        "     >                              
                     	       Elastic search URL Elastic search app credentials (Optional) Elastic search app name Elastic search fields Elastics Search Elastics Search Config Elasticserach Configuration Manage the sections and fields Elastics search index. Settings exp: TOKEN exp: http://localhost:9200 exp: oer_data Project-Id-Version: OERSI
PO-Revision-Date: 2022-06-30 17:29+0200
Last-Translator: 
Language-Team: edmondikacaj@gmail.com
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html;esc_html__
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: assets
X-Poedit-SearchPathExcluded-1: constant
X-Poedit-SearchPathExcluded-2: logs
X-Poedit-SearchPathExcluded-3: node_modules
X-Poedit-SearchPathExcluded-4: vendor
X-Poedit-SearchPathExcluded-5: packages
X-Poedit-SearchPathExcluded-6: .git
X-Poedit-SearchPathExcluded-7: .vscode
 Elastische Such-URL Elastic Search-Anmeldeinformationen (optional) Elastische Suche App-Name Elastische Suchfelder Elastische Suche Elastics Suche Konfiguration Elasticserach-Konfiguration Verwalten Sie die Abschnitte und Felder Elastics Suchindex. Einstellungen z.B: TOKEN z.B : http://localhost:9200 z.B.: oer_data 